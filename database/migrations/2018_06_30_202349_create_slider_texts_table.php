<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSliderTextsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('slider_texts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('big_text', 500)->nullable();
            $table->string('small_text', 500)->nullable();
            $table->string('big_text_am', 500)->nullable();
            $table->string('small_text_am', 500)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('slider_texts');
    }
}
