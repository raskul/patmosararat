@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Footer
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($footer, ['route' => ['footers.update', $footer->id], 'method' => 'patch']) !!}

                        @include('footers.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection