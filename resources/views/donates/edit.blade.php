@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Donate
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($donate, ['route' => ['donates.update', $donate->id], 'method' => 'patch']) !!}

                        @include('donates.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection