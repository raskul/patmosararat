@extends('layouts.index')
@section('content')

    @include('partials.header')


    <!--team-->
    <div class="team" id="STAFF" style="background-color: whitesmoke">
        <div class="container">
            <h3 class="title clr" style="color: black">Our Staff</h3>

            @include('partials.staff')

        </div>
    </div>


    @include('partials.footer')

@endsection