@extends('layouts.index')
@section('header')
    <style>
        .char-mid-grid p {
            color: black
        }

    </style>
@endsection
@push('header')
    <link rel="stylesheet" href="{{asset('Datetime-Picker-jQuery-Moment/css/datetimepicker.css')}}">
@endpush
@section('content')

    @include('partials.header')

    <br>
    <br>
    <br>
    <h3 class="title clr" style="color: black">Volunteer</h3>

    {!! Form::open(['route'=>['volunteerApply'], 'method' => 'POST', 'files' => true ]) !!}

    @include('flash::message')

    <div class="col-md-6 col-sm-6 col-xs-6 char-mid-grid" style="background-color: whitesmoke; color: black">
        <p>
        <div class="form-group {{ $errors->has('firstName') ? ' has-error' : '' }}">
            @if(! $lang)
                {!! Form::label('firstName', 'First Name:') !!}
            @else
                {!! Form::label('firstName', 'Անուն:') !!}
            @endif
            {!! Form::text('firstName', null, ['class' => 'form-control']) !!}
            @if ($errors->has('firstName'))
                <span class="help-block"><strong>{{ $errors->first('firstName') }}</strong></span>
            @endif
        </div>
        </p>
        <div class="clearfix"></div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-6 char-mid-grid" style="background-color: whitesmoke; color: black">
        <p>
        <div class="form-group {{ $errors->has('lastName') ? ' has-error' : '' }}">
            @if(! $lang)
                {!! Form::label('lastName', 'Last Name:') !!}
            @else
                {!! Form::label('lastName', 'Ազգանուն:') !!}
            @endif
            {!! Form::text('lastName', null, ['class' => 'form-control']) !!}
            @if ($errors->has('lastName'))
                <span class="help-block"><strong>{{ $errors->first('lastName') }}</strong></span>
            @endif
        </div>
        </p>
        <div class="clearfix"></div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-6 char-mid-grid" style="background-color: whitesmoke; color: black">
        <p>
        <div class="form-group {{ $errors->has('mobile') ? ' has-error' : '' }}">
            @if(! $lang)
                {!! Form::label('mobile', 'Mobile:') !!}
            @else
                {!! Form::label('mobile', 'շարժական:') !!}
            @endif
            {!! Form::text('mobile', null, ['class' => 'form-control']) !!}
            @if ($errors->has('mobile'))
                <span class="help-block"><strong>{{ $errors->first('mobile') }}</strong></span>
            @endif
        </div>
        </p>
        <div class="clearfix"></div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-6 char-mid-grid" style="background-color: whitesmoke; color: black">
        <p>
        <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
            @if(! $lang)
                {!! Form::label('email', 'Email:') !!}
            @else
                {!! Form::label('email', 'Email:') !!}
            @endif
            {!! Form::text('email', null, ['class' => 'form-control']) !!}
            @if ($errors->has('email'))
                <span class="help-block"><strong>{{ $errors->first('email') }}</strong></span>
            @endif
        </div>
        </p>
        <div class="clearfix"></div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-6 char-mid-grid" style="background-color: whitesmoke; color: black">
        <p>
        <div class="form-group {{ $errors->has('country') ? ' has-error' : '' }}">
            @if(! $lang)
                {!! Form::label('country', 'Country:') !!}
            @else
                {!! Form::label('country', 'Երկիրը:') !!}
            @endif
            {!! Form::text('country', null, ['class' => 'form-control']) !!}
            @if ($errors->has('country'))
                <span class="help-block"><strong>{{ $errors->first('country') }}</strong></span>
            @endif
        </div>
        </p>
        <div class="clearfix"></div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-6 char-mid-grid" style="background-color: whitesmoke; color: black">
        <p>
        <div class="form-group {{ $errors->has('address') ? ' has-error' : '' }}">
            @if(! $lang)
                {!! Form::label('address', 'Address:') !!}
            @else
                {!! Form::label('address', 'Հասցե:') !!}
            @endif
            {!! Form::text('address', null, ['class' => 'form-control']) !!}
            @if ($errors->has('address'))
                <span class="help-block"><strong>{{ $errors->first('address') }}</strong></span>
            @endif
        </div>
        </p>
        <div class="clearfix"></div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-6 char-mid-grid" style="background-color: whitesmoke; color: black">
        <p>
        <div class="form-group {{ $errors->has('birthday') ? ' has-error' : '' }}" style="height: 59px">
            @if(! $lang)
                {!! Form::label('birth-day', 'Birth Date:') !!}<br>
            @else
                {!! Form::label('birth-day', 'Ծննդյան օր:') !!}<br>
            @endif
            {{--            {!! Form::text('birthday', null, ['class' => 'form-control']) !!}--}}
            <select name="year" id="" class="form-control" style="width: 32%">
                @for($i = 115; $i >0; $i--)
                    @if($i == 115)
                        <option value="1800" selected>Year</option>
                    @endif
                    <option value="{{$i+1900}}">{{$i+1900}}</option>
                @endfor
            </select>
            <select name="month" id="" class="form-control" style="width: 32%">
                <option value="00" selected>Month</option>
                <option value="01">January</option>
                <option value="02">February</option>
                <option value="03">March</option>
                <option value="04">April</option>
                <option value="05">May</option>
                <option value="06">June</option>
                <option value="07">July</option>
                <option value="08">August</option>
                <option value="09">September</option>
                <option value="10">October</option>
                <option value="11">November</option>
                <option value="12">December</option>
            </select>
            <select name="day" id="" class="form-control" style="width: 32%">
                @for($i = 1; $i<= 31; $i++)
                    @if( $i == 1 )
                    <option value="00">Day</option>
                    @endif
                    <option value="{{$i}}">{{$i}}</option>
                @endfor
            </select>

            @if ($errors->has('birthday'))
                <span class="help-block"><strong>{{ $errors->first('birthday') }}</strong></span>
            @endif
        </div>
        </p>
        <div class="clearfix"></div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-6 char-mid-grid" style="background-color: whitesmoke; color: black">
        <p style="height: 45px"></p>
        <p>
            @if(! $lang)
                If accepted when would you like your volunteer to:
            @else
                Եթե ընդունվի, երբ կցանկանայիք ձեր կամավորին
            @endif
        </p>
        <div class="clearfix"></div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-6 char-mid-grid" style="background-color: whitesmoke; color: black">
        <p>
        <div class="form-group {{ $errors->has('startDate') ? ' has-error' : '' }}">
            @if(! $lang)
                {!! Form::label('startDate', 'Start Date:') !!}
            @else
                {!! Form::label('startDate', 'Մեկնարկի ամսաթիվ:') !!}
            @endif
                <div id="picker"></div>
                <input type="hidden" id="res1" value="2017-01-01 00:00" name="startDate">
                {{--    {!! Form::text('time', null, ['class' => 'form-control', 'type' => 'hidden', 'id' => 'result']) !!}--}}

            @if ($errors->has('startDate'))
                <span class="help-block"><strong>{{ $errors->first('startDate') }}</strong></span>
            @endif
        </div>
        </p>
        <div class="clearfix"></div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-6 char-mid-grid" style="background-color: whitesmoke; color: black">
        <p>
        <div class="form-group {{ $errors->has('endDate') ? ' has-error' : '' }}">
            @if(! $lang)
                {!! Form::label('endDate', 'End Date:') !!}
            @else
                {!! Form::label('endDate', 'վերջ Ամսաթիվ:') !!}
            @endif
                <div id="picker2"></div>
                <input type="hidden" id="res2" value="2017-01-01 00:00" name="endDate">
                {{--    {!! Form::text('time', null, ['class' => 'form-control', 'type' => 'hidden', 'id' => 'result']) !!}--}}

            @if ($errors->has('endDate'))
                <span class="help-block"><strong>{{ $errors->first('endDate') }}</strong></span>
            @endif
        </div>
        </p>
        <div class="clearfix"></div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-6 char-mid-grid" style="background-color: whitesmoke; color: black">
        <p>
            @if(! $lang)
                {!! Form::label('gender', 'Gender:') !!}
            @else
            {!! Form::label('gender', 'Սեռը:') !!}
        @endif
        <div><input class="" type="radio" name="gender" value="male" checked><label>@if(! $lang) Male @else տղամարդիկ @endif</label></div>
        <div><input class="" type="radio" name="gender" value="female"><label>@if(! $lang) Female @else իգական @endif</label></div>
        <div><input class="" type="radio" name="gender" value="other"><label>@if(! $lang) Other @else Այլ @endif</label></div>
        </p>
        <div class="clearfix"></div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-6 char-mid-grid" style="background-color: whitesmoke; color: black">
        <p>
        {{--<input class="form-control" type="file" name="">--}}
        <div class="form-group {{ $errors->has('cv') ? ' has-error' : '' }}">
            @if(! $lang)
                {!! Form::label('cv', 'Upload CV:') !!}
            @else
                {!! Form::label('cv', 'Վերբեռնեք CV:') !!}
            @endif
            {!! Form::file('cv', ['class' => 'form-control']) !!}
            @if ($errors->has('cv'))
                <span class="help-block"><strong>{{ $errors->first('cv') }}</strong></span>
            @endif
            <span style="font-size: small">pdf, jpg, jpeg, zip</span>
        </div>
        </p>
        <div class="clearfix"></div>
    </div>
    <div class="clearfix"></div>
    <div class="col-md-12 char-mid-grid" style="background-color: whitesmoke; color: black">
        <p style="text-align: center">
            <button class="btn btn-success btn-lg">
                @if(! $lang)
                    Send
                @else
                    Ուղարկել
                @endif
            </button>
        </p>
        <div class="clearfix"></div>
    </div>

    {!! Form::close() !!}

    <div class="clearfix"></div>

    @include('partials.footer')

@endsection
@push('scripts')
    <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha384-nvAa0+6Qg9clwYCGGPpDQLVpLNn0fRaROjHqs13t4Ggj3Ez50XnGQqc/r8MhnRDZ" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/moment-with-locales.min.js"></script>
    <script src="{{asset('Datetime-Picker-jQuery-Moment/js/datetimepicker.js')}}"></script>
    <script>
        $('#picker').dateTimePicker({
            selectData: "now",
            dateFormat: "YYYY-MM-DD HH:mm",
            showTime: false,
            locale: 'en',
            positionShift: { top: 20, left: 0},
            title: "Select Date and Time",
            buttonTitle: "Select"
        });
    </script>
    <script>
        $('#picker2').dateTimePicker({
            selectData: "now",
            dateFormat: "YYYY-MM-DD HH:mm",
            showTime: false,
            locale: 'en',
            positionShift: { top: 20, left: 0},
            title: "Select Date and Time",
            buttonTitle: "Select"
        });
    </script>
@endpush