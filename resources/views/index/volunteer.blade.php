@extends('layouts.index')
@section('header')
    <style>
        .char-mid-grid p {
            color: black
        }
    </style>
@endsection
@section('content')

    @include('partials.header')

    <br>
    <br>
    <br>
    <h3 class="title clr" style="color: black">Volunteer</h3>

    <div class="col-sm-12 " style="background-color: whitesmoke; color: black">
        <p>
            @if(! $lang)
                {!! $volunteer->text !!}
            @else
                {!! $volunteer->text_am !!}
            @endif
        </p>
        <div style="text-align: center">
            <a href="{{route('volunteerApply')}}" class="btn btn-primary" style="color: black;margin: 40px;"><strong>Apply
                    Now</strong></a>
        </div>

        <div class="clearfix"></div>
        <div style="height: 60px"></div>
    </div>

    @include('partials.footer')

@endsection
