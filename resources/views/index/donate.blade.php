@extends('layouts.index')
@section('content')

    @include('partials.header')

    <!-- About Us -->
    <div class="about" id="about">
        <div class="container">
            @if(! $lang)
                {!! $donate{0}->text !!}
            @else
                {!! $donate{0}->text_am !!}
            @endif
        </div>
    </div>
    <!--//about-->

    @include('partials.footer')

@endsection