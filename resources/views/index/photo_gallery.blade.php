<!DOCTYPE html>
<html lang="EN">
<head>

    <title>Patmosararat</title>
    <!--meta tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script>
        addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>
    <!--//meta tags ends here-->
    <!--booststrap-->
    <link href="{{asset('css/bootstrap.css')}}" rel="stylesheet" type="text/css" media="all">
    <!--//booststrap end-->
    <!-- font-awesome icons -->
    <link href="{{asset('css/font-awesome.css')}}" rel="stylesheet">
    <!-- //font-awesome icons -->
    <link rel="stylesheet" href="{{asset('css/owl.carousel.css')}}" type="text/css" media="all">
    <!-- clients-->
    <!--stylesheets-->
    <link href="{{asset('css/style.css')}}" rel='stylesheet' type='text/css' media="all">
    <!--//stylesheets-->
    <link href="//fonts.googleapis.com/css?family=Montserrat:300,400,500" rel="stylesheet">


    <link rel="stylesheet" type="text/css" media="all" href="{{asset('lightbox/css/styles.css')}}">
    <link rel="stylesheet" type="text/css" media="all" href="{{asset('lightbox/css/jquery.lightbox-0.5.css')}}">
    <script type="text/javascript" src="{{asset('lightbox/js/jquery-1.10.1.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('lightbox/js/jquery.lightbox-0.5.min.js')}}"></script>

</head>


<div>

    @include('partials.header')


    <div class="news" id="Events">
        <h3 class="title">Photo Gallery</h3>
        <div class="blog-two-agile" id="">
            <div id="thumbnails">

                @php
                    $i = 0;
                @endphp
                @foreach($photos as $photo)
                    @if($i % 4 == 0)
                        <div class="blog-right-top">
                    @endif
                        <div class="col-md-3 col-xs-6 blog-rit-right ">
                            <div class="w3layouts_news_grid">
                                {{--<img src="{{route('index')}}/uploads/events/image/thumbnail/{{$photo->image}}" alt=" " class="img-responsive">--}}
                                <a href="{{route('index')}}/uploads/events/image/{{$photo->image}}" title="{{$photo->description}}">
                                    <img src="{{route('index')}}/uploads/events/image/thumbnail/{{$photo->image}}" alt="">
                                </a>
                            </div>
                        </div>
                    @if($i % 4 == 3)
                            <div class="clearfix"></div>
                        </div>
                    @endif
                    @php
                        $i++;
                    @endphp
                @endforeach
                @if(($i % 4 == 1) || ($i % 4 == 2))
                    <div class="blog-right-top">
                        <div class="col-md-3 col-xs-6 blog-rit-right ">
                            <div class="w3layouts_news_grid">
                                <div style="height: 300px"></div>
                            </div>
                        </div>
                    </div>
                    <div class="blog-right-top">
                        <div class="col-md-3 col-xs-6 blog-rit-right ">
                            <div class="w3layouts_news_grid">
                                <div style="height: 300px"></div>
                            </div>
                        </div>
                    </div>
                @endif
                @if($i <= 4)
                        <div class="clearfix"></div>
                    </div>
                @endif

                </div>
            <div class="clearfix"> </div>
        </div>
    </div>

    <div class="news" >
        <div class="container">
            <div class="clearfix"> </div>
            <div style="text-align: center;font-size: medium">{{$photos->render()}}</div>
            <div class="clearfix"> </div>
        </div>
    </div>
    <div class="clearfix"> </div>

    @include('partials.footer')

    <script src="{{asset('js/responsiveslides.min.js')}}"></script>
    <script>
        // You can also use "$(window).load(function() {"
        $(function () {
            // Slideshow 4
            $("#slider4").responsiveSlides({
                auto: true,
                pager: false,
                nav: true,
                speed: 900,
                namespace: "callbacks",
                before: function () {
                    $('.events').append("<li>before event fired.</li>");
                },
                after: function () {
                    $('.events').append("<li>after event fired.</li>");
                }
            });

        });
    </script>
    <!--// banner-->
    <!--client carousel -->
    <script src="{{asset('js/owl.carousel.js')}}"></script>
    <script>
        $(document).ready(function () {
            $("#owl-demo").owlCarousel({
                items: 1,
                itemsDesktop: [768, 1],
                itemsDesktopSmall: [414, 1],
                lazyLoad: true,
                autoPlay: true,
                navigation: true,

                navigationText: false,
                pagination: true,

            });

        });
    </script>
    <!-- start-smoth-scrolling -->
    <script  src="{{asset('js/move-top.js')}}"></script>
    <script  src="{{asset('js/easing.js')}}"></script>
    <script >
        jQuery(document).ready(function ($) {
            $(".scroll").click(function (event) {
                event.preventDefault();
                $('html,body').animate({ scrollTop: $(this.hash).offset().top }, 1000);
            });
        });
    </script>
    <!-- start-smoth-scrolling -->
    <!-- here stars scrolling icon -->
    <script>
        $(document).ready(function () {
            /*
                var defaults = {
                containerID: 'toTop', // fading element id
                containerHoverID: 'toTopHover', // fading element hover id
                scrollSpeed: 1200,
                easingType: 'linear'
                };
            */

            $().UItoTop({ easingType: 'easeOutQuart' });

        });
    </script>
    <!-- //here ends scrolling icon -->
    <!--bootstrap working-->
    <script src="{{asset('js/bootstrap.js')}}"></script>
    <!-- //bootstrap working-->

    <script type="text/javascript">
        $(function() {
            $('#thumbnails a').lightBox();
        });
    </script>

</body>



</html>