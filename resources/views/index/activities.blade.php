@extends('layouts.index')
@section('header')
    <style>
        .char-mid-grid p {
            color: black
        }
    </style>
@endsection
@section('content')

    @include('partials.header')

    <br>
    <br>
    <br>
    <h3 class="title clr" style="color: black">Activities</h3>

    <div class="col-md-6 col-sm-6 col-xs-6 char-mid-grid" style="background-color: whitesmoke; color: black">
        <p>
            @if(! $lang)
                {!! $activites{0}->left_text !!}
            @else
                {!! $activites{0}->left_text_am !!}
            @endif
        </p>
        <div class="clearfix"></div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-6 char-mid-grid" style="background-color: whitesmoke; color: black">
        <p>
            @if(! $lang)
                {!! $activites{0}->right_text !!}
            @else
                {!! $activites{0}->right_text_am !!}
            @endif
        </p>
        <div class="clearfix"></div>
    </div>
    <div class="clearfix"></div>

    @include('partials.footer')

@endsection