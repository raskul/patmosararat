@extends('layouts.index')
@section('header')
    <style>
        .char-mid-grid p {
            color: black
        }
    </style>
@endsection

@section('content')

    @include('partials.header')

    <br>
    <br>
    <br>
    <h3 class="title clr" style="color: black">Message</h3>


    <div class="col-md-12" style="background-color: white; color: black;">
        <p style="margin-top: 40px; margin-bottom: 70px">
            @include('flash::message')
        </p>
        <div class="clearfix"></div>
    </div>
    <div style="height: 70px"></div>


    <div class="clearfix"></div>

    @include('partials.footer')

@endsection
