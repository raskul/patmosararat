@extends('layouts.index')
@section('content')

    @include('partials.header')

    <!-- About Us -->
    <div class="about" id="about">
        <div class="container">
            @if(! $lang)
                {!! $contact{0}->text !!}
            @else
                {!! $contact{0}->text_am !!}
            @endif
        </div>
    </div>
    <!--//about-->

    @include('partials.footer')

@endsection