@extends('layouts.index')
@section('content')

    @include('partials.header')

    <!-- //banner -->
    <!-- modal -->
    {{--<div class="modal about-modal fade" id="myModal" tabindex="-1" role="dialog">--}}
    {{--<div class="modal-dialog" role="document">--}}
    {{--<div class="modal-content">--}}
    {{--<div class="modal-header">--}}
    {{--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span--}}
    {{--aria-hidden="true">&times;</span></button>--}}
    {{--<h4 class="modal-title">GoodWill</h4>--}}
    {{--</div>--}}
    {{--<div class="modal-body">--}}
    {{--<div class="out-info">--}}
    {{--<img src="images/b2.jpg" alt=""/>--}}
    {{--<p>Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor--}}
    {{--eu, consequat vitae,--}}
    {{--eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellu--}}
    {{--</p>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    <!-- //modal -->
    <!-- About Us -->
    <div class="about" id="about">
        <div class="container">
            <div class="col-md-5 w3l_aboutdown5">
                <div class="aboutright">
                    <h4> @if(! $lang) About us @else Մեր մասին @endif</h4>
                </div>
                <div class="about-jst-right">
                    <p>
                        @if(! $lang)
                            {!! substr($about_us{0}->text, 0 ,600) !!}
                            <br><span><a style="color: blue" href="{{route('about_us')}}">Read More...</a></span>
                        @else
                            {!! substr($about_us{0}->text_am, 0 ,600) !!}
                            <br><span><a style="color: blue" href="{{route('about_us')}}">Կարդալ ավելին...</a></span>
                        @endif
                    </p>
                </div>
            </div>
            <div class="col-md-7 aboutside">
                {{--<div class="aboutdown">--}}
                {{--<div class="jst-right">--}}
                {{--<div class="pos-top">--}}
                {{--<h4>Vivamus elementum</h4>--}}
                {{--<p>Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula,--}}
                {{--porttitor eu, consequat vitae,</p>--}}
                {{--</div>--}}
                {{--<div class="pos-top">--}}
                {{--<h4>Vivamus elementum</h4>--}}
                {{--<p>Quis autem vel eum iure reprehderit.Quis autem vel eum iure reprehderit.--}}
                {{--eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis,--}}
                {{--</p>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                <div class="aboutdown1">
                    <img src="{{route('index')}}/images/2.jpg" class="img-responsive" alt="" style="height: 250px">
                    <div class="aboutimgflex">
                        <div class="col-md-6 col-sm-6 col-xs-6 imgflex">
                            <img src="{{route('index')}}/images/4.jpg" class="img-responsive" alt=""
                                 style="height: 250px">
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6 imgflex1">
                            <img src="{{route('index')}}/images/3.jpg" class="img-responsive" alt=""
                                 style="height: 250px">
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--//about-->
    <!--services-->
    <div class="services" id="services">
        {{--<h3 class="title clr">Services</h3>--}}
        {{--<div class="banner-bottom-girds ">--}}
            {{--@php--}}
                {{--$i = 1;--}}
            {{--@endphp--}}
            {{--@foreach($services as $service)--}}
                {{--<div class="col-md-3  col-sm-6 col-xs-6  its-banner-grid">--}}
                    {{--<div class="col-md-3 left-icon-grid">--}}
                        {{--<span class="fa fa-check banner-icon" aria-hidden="true"></span>--}}
                    {{--</div>--}}
                    {{--<div class="col-md-9 white-shadow">--}}
                        {{--<h4>@if(! $lang) {{$service->big_text}} @else {{$service->big_text_am}} @endif</h4>--}}
                        {{--<p>--}}
                            {{--@if(! $lang) {{$service->small_text}} @else {{$service->small_text_am}} @endif--}}
                        {{--</p>--}}
                        {{--<div class="clearfix"></div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--@if($i % 4 == 0)--}}
                    {{--<div class="clearfix"></div>--}}
                {{--@endif--}}
                {{--@php--}}
                    {{--$i++;--}}
                {{--@endphp--}}
            {{--@endforeach--}}
        {{--</div>--}}

        <!--//activities-->
        <br>
        <br>
        <br>
        <h3 class="title clr">Activities</h3>
        <div class="banner-bottom-girds ">
            <div class="col-md-6 col-sm-6 col-xs-6 char-mid-grid">
                <p>
                    @if(! $lang)
                        {!! $activites{0}->left_text !!}
                    @else
                        {!! $activites{0}->left_text_am !!}
                    @endif
                </p>
                <div class="clearfix"></div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-6 char-mid-grid">
                <p>
                    @if(! $lang)
                        {!! $activites{0}->right_text !!}
                    @else
                        {!! $activites{0}->right_text_am !!}
                    @endif
                </p>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
        <!--//end activities-->
        <div class="banner-bottom-girds ">
            <div class="clearfix"></div>
        </div>
    </div>
    <!--//services-->
    <!--Events-->
    <div class="news" id="Events">
        <h3 class="title">Our Event</h3>
        <div class="blog-two-agile">
            @php
                $i = 1;
            @endphp

            @foreach($events as $event)
                @if(($i == 1))
                    <div class="blog-right-top">
                @endif
                @if(($i == 5))
                    <div class="blog-right">
                @endif
                    <div class="col-md-3 col-xs-6 blog-rit-right ">
                        <div class="w3layouts_news_grid">
                            <img src="{{route('index')}}/uploads/events/image/thumbnail/{{$event->image}}" alt=" " class="img-responsive">
                        </div>
                        <div class="outs_news_grid text-center clr-1">
                            <h6>{{$event->name}}</h6>
                            <ul>
                                <li><span class="fa fa-calendar" aria-hidden="true"></span>
                                    @php
                                        $time = strtotime($event->time);
                                    @endphp
                                    {{date('d F Y', $time)}}
                                </li>
                                <li><span class="fa fa-clock-o" aria-hidden="true"></span>
                                    {{date('h:i A', $time)}}
                                </li>
                            </ul>
                            <a href="{{route('photo_gallery', $event->id)}}" class="blog-more">
                                @if(! $lang)Read More @elseԿարդալ ավելին @endif
                            </a>
                        </div>
                    </div>
                @if(($i == 4)||($i == 8))
                        <div class="clearfix"></div>
                    </div>
                @endif
                @php
                    $i++;
                @endphp
                @endforeach
                @if($i<9)
                        <div class="clearfix"></div>
                    </div>
                @endif
                        {{--<div class="col-md-3 col-xs-6 blog-rit-right ">--}}
                        {{--<div class="w3layouts_news_grid">--}}
                        {{--<img src="images/b2.jpg" alt=" " class="img-responsive">--}}

                        {{--</div>--}}
                        {{--<div class="outs_news_grid text-center clr-2">--}}
                        {{--<h6>Lorem ipsum</h6>--}}
                        {{--<ul>--}}
                        {{--<li><span class="fa fa-calendar" aria-hidden="true"></span>28 March 2017</li>--}}
                        {{--<li><span class="fa fa-clock-o" aria-hidden="true"></span>9:00 AM</li>--}}
                        {{--</ul>--}}
                        {{--<a href="#" class="blog-more" data-toggle="modal" data-target="#myModal">Read More</a>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="col-md-3 col-xs-6 blog-rit-right ">--}}
                        {{--<div class="w3layouts_news_grid">--}}
                        {{--<img src="images/b3.jpg" alt=" " class="img-responsive">--}}

                        {{--</div>--}}
                        {{--<div class="outs_news_grid text-center clr-1">--}}
                        {{--<h6>Lorem ipsum</h6>--}}
                        {{--<ul>--}}
                        {{--<li><span class="fa fa-calendar" aria-hidden="true"></span>28 March 2017</li>--}}
                        {{--<li><span class="fa fa-clock-o" aria-hidden="true"></span>9:00 AM</li>--}}
                        {{--</ul>--}}
                        {{--<a href="#" class="blog-more" data-toggle="modal" data-target="#myModal">Read More</a>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="col-md-3 col-xs-6 blog-rit-right ">--}}
                        {{--<div class="w3layouts_news_grid">--}}
                        {{--<img src="images/b4.jpg" alt=" " class="img-responsive">--}}

                        {{--</div>--}}
                        {{--<div class="outs_news_grid text-center clr-2">--}}
                        {{--<h6>Lorem ipsum</h6>--}}
                        {{--<ul>--}}
                        {{--<li><span class="fa fa-calendar" aria-hidden="true"></span>28 March 2017</li>--}}
                        {{--<li><span class="fa fa-clock-o" aria-hidden="true"></span>9:00 AM</li>--}}
                        {{--</ul>--}}
                        {{--<a href="#" class="blog-more" data-toggle="modal" data-target="#myModal">Read More</a>--}}
                        {{--</div>--}}
                        {{--</div>--}}


                        {{--<div class="clearfix"></div>--}}
                    {{--</div>--}}
                    {{--<div class="blog-right">--}}
                        {{--<div class="col-md-3 col-xs-6 blog-rit-right ">--}}
                            {{--<div class="w3layouts_news_grid">--}}
                                {{--<img src="images/b3.jpg" alt=" " class="img-responsive">--}}

                            {{--</div>--}}
                            {{--<div class="outs_news_grid text-center clr-1">--}}
                                {{--<h6>Lorem ipsum5</h6>--}}
                                {{--<ul>--}}
                                    {{--<li><span class="fa fa-calendar" aria-hidden="true"></span>28 March 2017</li>--}}
                                    {{--<li><span class="fa fa-clock-o" aria-hidden="true"></span>9:00 AM</li>--}}
                                {{--</ul>--}}
                                {{--<a href="#" class="blog-more" data-toggle="modal" data-target="#myModal">Read More</a>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="col-md-3 col-xs-6 blog-rit-right ">--}}
                            {{--<div class="w3layouts_news_grid">--}}
                                {{--<img src="images/b11.jpg" alt=" " class="img-responsive">--}}

                            {{--</div>--}}
                            {{--<div class="outs_news_grid text-center clr-2">--}}
                                {{--<h6>Lorem ipsum</h6>--}}
                                {{--<ul>--}}
                                    {{--<li><span class="fa fa-calendar" aria-hidden="true"></span>28 March 2017</li>--}}
                                    {{--<li><span class="fa fa-clock-o" aria-hidden="true"></span>9:00 AM</li>--}}
                                {{--</ul>--}}
                                {{--<a href="#" class="blog-more" data-toggle="modal" data-target="#myModal">Read More</a>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="col-md-3 col-xs-6 blog-rit-right ">--}}
                            {{--<div class="w3layouts_news_grid">--}}
                                {{--<img src="images/b4.jpg" alt=" " class="img-responsive">--}}

                            {{--</div>--}}
                            {{--<div class="outs_news_grid text-center clr-1">--}}
                                {{--<h6>Lorem ipsum</h6>--}}
                                {{--<ul>--}}
                                    {{--<li><span class="fa fa-calendar" aria-hidden="true"></span>28 March 2017</li>--}}
                                    {{--<li><span class="fa fa-clock-o" aria-hidden="true"></span>9:00 AM</li>--}}
                                {{--</ul>--}}
                                {{--<a href="#" class="blog-more" data-toggle="modal" data-target="#myModal">Read More</a>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="col-md-3 col-xs-6 blog-rit-right ">--}}
                            {{--<div class="w3layouts_news_grid">--}}
                                {{--<img src="images/b2.jpg" alt=" " class="img-responsive">--}}

                            {{--</div>--}}
                            {{--<div class="outs_news_grid text-center clr-2">--}}
                                {{--<h6>Lorem ipsum</h6>--}}
                                {{--<ul>--}}
                                    {{--<li><span class="fa fa-calendar" aria-hidden="true"></span>28 March 2017</li>--}}
                                    {{--<li><span class="fa fa-clock-o" aria-hidden="true"></span>9:00 AM</li>--}}
                                {{--</ul>--}}
                                {{--<a href="#" class="blog-more" data-toggle="modal" data-target="#myModal">Read More</a>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="clearfix"></div>--}}
                    {{--</div>--}}
                    <div class="clearfix"></div>
        </div>
    </div>
    <!--//Events-->
    <!--team-->
    <div class="team" id="STAFF">
        <div class="container">
            <h3 class="title clr">Our Staff</h3>

            @include('partials.staff')

        </div>
    </div>
    <!--testimonials-->
    {{--<div class="testimonials" id="testimonials">--}}
    {{--<h3 class="title tit-clr">Our Donors</h3>--}}
    {{--<div class="container">--}}
    {{--<div class="clients-inn">--}}
    {{--<div class="col-md-6 clients_agile_slider">--}}
    {{--<div id="owl-demo" class="owl-carousel owl-theme">--}}
    {{--<div class="item">--}}
    {{--<div class="agile_clients_content">--}}
    {{--<div class="a-midd-main">--}}
    {{--<img class="agile-img" src="images/c1.jpg" alt="img">--}}
    {{--<h4>Ketty way</h4>--}}
    {{--<p> Lorem ipsum adipiscing elit, sed do eiusmod idunt ut labore. sed do eiusmod </p>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--<div class="item">--}}
    {{--<div class="agile_clients_content">--}}
    {{--<div class="a-midd-main">--}}
    {{--<img class="agile-img" src="images/c2.jpg" alt="img">--}}
    {{--<h4>Cleark Hill</h4>--}}
    {{--<p> Lorem ipsum adipiscing elit, sed do eiusmod idunt ut labore. sed do eiusmod </p>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--<div class="item">--}}
    {{--<div class="agile_clients_content">--}}
    {{--<div class="a-midd-main">--}}
    {{--<img class="agile-img" src="images/c3.jpg" alt="img">--}}
    {{--<h4>Willson Doe</h4>--}}
    {{--<p> Lorem ipsum adipiscing elit, sed do eiusmod idunt ut labore. sed do eiusmod </p>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--<div class="clearfix"></div>--}}
    {{--</div>--}}
    {{--<div class="col-md-6 clients_right">--}}
    {{--<form action="#" method="post">--}}
    {{--<div class="main">--}}
    {{--<div class="col-md-6 col-sm-6 col-xs-6 form-left-to-w3l">--}}
    {{--<input type="text" name="name" placeholder="Name" required="">--}}
    {{--<div class="clear"></div>--}}
    {{--</div>--}}
    {{--<div class="col-md-6 col-sm-6 col-xs-6 form-right-to-w3ls">--}}
    {{--<input type="text" name="last name" placeholder="Last Name" required="">--}}
    {{--<div class="clear"></div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--<div class="main">--}}
    {{--<div class="col-md-6 col-sm-6 col-xs-6 form-left-to-w3l">--}}
    {{--<input type="email" name="email" required="" placeholder="Email">--}}
    {{--</div>--}}
    {{--<div class="col-md-6 col-sm-6 col-xs-6 form-right-to-w3ls ">--}}
    {{--<input type="text" name="phone number" placeholder="Phone Number" required="">--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--<div class="main">--}}
    {{--<div class="col-md-6 col-sm-6 col-xs-6 form-left-to-w3l">--}}
    {{--<select class="form-control buttom">--}}
    {{--<option value="">--}}
    {{--Donation Type--}}
    {{--</option>--}}
    {{--<option value="category2">Once-Off-Gift</option>--}}
    {{--<option value="category1">Regular-Monthly-Gift</option>--}}
    {{--</select>--}}
    {{--</div>--}}
    {{--<div class="col-md-6 col-sm-6 col-xs-6 form-right-to-w3ls">--}}
    {{--<select class="form-control buttom">--}}
    {{--<option value="">--}}
    {{--Donation Amount--}}
    {{--</option>--}}
    {{--<option value="category2">200$</option>--}}
    {{--<option value="category1">150$</option>--}}
    {{--<option value="category3">400$</option>--}}
    {{--</select>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--<div class="btnn">--}}
    {{--<button type="submit">Send</button>--}}
    {{--<br>--}}
    {{--</div>--}}
    {{--</form>--}}
    {{--</div>--}}
    {{--<div class="clearfix"></div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    <!-- //testimonials-->
    <!-- Contact-form -->
    {{--<div class="contact" id="contact">--}}
    {{--<div class="container">--}}
    {{--<h3 class="title clr">Contact us</h3>--}}
    {{--<div class="w3layouts_mail_grid_right">--}}
    {{--<form action="#" method="post">--}}
    {{--<div class="col-md-6 col-xs-6 wthree_contact_left_grid">--}}
    {{--<input type="text" name="Name" placeholder="Name" required="">--}}
    {{--</div>--}}
    {{--<div class="col-md-6 col-xs-6 wthree_contact_left_grid">--}}
    {{--<input type="email" name="Email" placeholder="Email" required="">--}}
    {{--</div>--}}
    {{--<div class="col-md-6 col-xs-6 wthree_contact_left_grid">--}}
    {{--<input type="text" name="Telephone" placeholder="Telephone" required="">--}}
    {{--</div>--}}
    {{--<div class="col-md-6 col-xs-6 wthree_contact_left_grid">--}}
    {{--<input type="text" name="Subject" placeholder="Subject" required="">--}}
    {{--</div>--}}
    {{--<div class="clearfix"></div>--}}
    {{--<textarea name="Message" placeholder="Message..." required=""></textarea>--}}
    {{--<input type="submit" value="Submit">--}}
    {{--<input type="reset" value="Clear">--}}
    {{--</form>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--<div class="address-below">--}}
    {{--<div class="contact-icons text-center">--}}
    {{--<div class="col-md-4 col-sm-4 col-xs-4 footer_grid_left">--}}
    {{--<div class="icon_grid_left">--}}
    {{--<span class="fa fa-map-marker" aria-hidden="true"></span>--}}
    {{--</div>--}}
    {{--<div class="address-gried">--}}
    {{--<p>333 Broome St<span>New York,Ny 10002,</span></p>--}}
    {{--</div>--}}
    {{--<div class="clearfix"></div>--}}
    {{--</div>--}}
    {{--<div class="col-md-4 col-sm-4 col-xs-4 footer_grid_left">--}}
    {{--<div class="icon_grid_left">--}}
    {{--<span class="fa fa-volume-control-phone" aria-hidden="true"></span>--}}
    {{--</div>--}}
    {{--<div class="address-gried">--}}
    {{--<p>+(000) 123 4565 32 <span>+(010) 123 4565 35</span></p>--}}
    {{--</div>--}}
    {{--<div class="clearfix"></div>--}}
    {{--</div>--}}
    {{--<div class="col-md-4 col-sm-4 col-xs-4 footer_grid_left">--}}
    {{--<div class="icon_grid_left">--}}
    {{--<span class="fa fa-envelope" aria-hidden="true"></span>--}}
    {{--</div>--}}
    {{--<div class="address-gried">--}}
    {{--<p><a href="mailto:info@example.com">info@example1.com</a>--}}
    {{--<span><a href="mailto:info@example.com">info@example2.com</a></span>--}}
    {{--</p>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--<div class="clearfix"></div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    <!--map-->
    {{--<div class="map">--}}
    {{--<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3023.9503398796587!2d-73.9940307!3d40.719109700000004!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c25a27e2f24131%3A0x64ffc98d24069f02!2sCANADA!5e0!3m2!1sen!2sin!4v1441710758555"></iframe>--}}
    {{--</div>--}}
    <!-- //map-->
    <!-- //Contact-form -->
    <!--footer-->

    @include('partials.footer')

@endsection