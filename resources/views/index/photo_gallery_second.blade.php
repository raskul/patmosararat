@extends('layouts.index')
@section('header')
    <style>
        .char-mid-grid p {
            color: black
        }
    </style>
@endsection
@section('content')

    @include('partials.header')


    <div class="news" id="Events">
        <h3 class="title">Photo Gallery</h3>
        <div class="blog-two-agile" id="">
            <div id="thumbnails">

                @php
                    $i = 0;
                @endphp
                @foreach($events as $event)
                    @if($i % 4 == 0)
                        <div class="blog-right-top">
                    @endif

                        <div class="col-md-3 col-xs-6 blog-rit-right ">
                            <div class="w3layouts_news_grid">
                                <img src="{{route('index')}}/uploads/events/image/thumbnail/{{$event->image}}" alt=" " class="img-responsive">
                            </div>
                            <div class="outs_news_grid text-center clr-1">
                                <h6>{{$event->name}}</h6>
                                <ul>
                                    <li><span class="fa fa-calendar" aria-hidden="true"></span>
                                        @php
                                            $time = strtotime($event->time);
                                        @endphp
                                        {{date('d F Y', $time)}}
                                    </li>
                                    <li><span class="fa fa-clock-o" aria-hidden="true"></span>
                                        {{date('h:i A', $time)}}
                                    </li>
                                </ul>
                                <a href="{{route('photo_gallery', $event->id)}}" class="blog-more">
                                    @if(! $lang)Read More @elseԿարդալ ավելին @endif
                                </a>
                            </div>
                        </div>
                    @if($i % 4 == 3)
                            <div class="clearfix"></div>
                        </div>
                    @endif
                    @php
                        $i++;
                    @endphp
                @endforeach
                @if(($i % 4 == 1) || ($i % 4 == 2))
                    <div class="blog-right-top">
                        <div class="col-md-3 col-xs-6 blog-rit-right ">
                            <div class="w3layouts_news_grid">
                                <div style="height: 300px"></div>
                            </div>
                        </div>
                    </div>
                    <div class="blog-right-top">
                        <div class="col-md-3 col-xs-6 blog-rit-right ">
                            <div class="w3layouts_news_grid">
                                <div style="height: 300px"></div>
                            </div>
                        </div>
                    </div>
                @endif
                @if($i <= 4)
                        <div class="clearfix"></div>
                    </div>
                @endif

                </div>
            <div class="clearfix"> </div>
        </div>
    </div>

    <div class="news" >
        <div class="container">
            <div class="clearfix"> </div>
            <div style="text-align: center;font-size: medium">{{$events->render()}}</div>
            <div class="clearfix"> </div>
        </div>
    </div>
    <div class="clearfix"> </div>

    @include('partials.footer')

@endsection
@push('scripts')
    <script>
        $(function() {
            $('#thumbnails a').lightBox();
        });
    </script>
@endpush