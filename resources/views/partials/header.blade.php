@php
    if (Cookie::get('lang') == 'am'){
        Cookie::queue(Cookie::forever('lang', 'am'));
        $lang = 'am';
    }else{
        Cookie::queue(Cookie::forget('lang'));
        $lang = false;
    }
@endphp
<div class="header-outs" id="home">
    <div class="header-w3layouts">
        <!-- Navigation -->
        <div class="header-bar">
            <nav class="navbar navbar-default">
                <div class="navbar-header navbar-left">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    @if($lang)
                        <a href="{{route('lang_en')}}"> <span style="color: white; font-size: small"> change language to English </span><img
                                    src="{{route('index')}}/images/flag.png" style="width: 20px" alt=""></a>
                    @else
                        <a href="{{route('lang_am')}}"> <span style="color: white; font-size: small"> change language to Armenian </span><img
                                    src="{{route('index')}}/images/amflag.png" style="width: 20px" alt=""></a>
                    @endif
                    <br>
                    <h1>
                        <a style="text-transform: none" class="navbar-brand" href="{{route('index')}}">@if(! $lang) Patmos-Ararat @else պատմոս-արարատ @endif</a>
                    </h1>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
                    <nav>
                        <ul class="nav navbar-nav">
                            <li class="@if(Request::url() == route('index')) active @endif"><a href="{{route('index')}}">@if(! $lang)Home @elseտուն @endif</a></li>
                            <li class="@if(Request::url() == route('about_us')) active @endif"><a href="{{route('about_us')}}">@if(! $lang)About us @elseՄԱՍԻՆ @endif</a></li>
                            @if(Request::url() == route('index'))
                            <li><a href="#services" class="scroll">@if(! $lang)Activities @else
                                        գործունեությունը @endif</a></li>
                            @else
                            <li class="@if(Request::url() == route('activities')) active @endif"><a href="{{route('activities')}}" >@if(! $lang)Activities @else
                                        գործունեությունը @endif</a></li>
                            @endif
                            <li class="@if(Request::url() == route('photo_gallery')) active @endif"><a href="{{route('photo_gallery')}}" >@if(! $lang)Photo gallery @else
                                        լուսանկարներ @endif</a></li>
                            <li class="@if((Request::url() == route('volunteer')) || (Request::url() == route('volunteerApply'))) active @endif"><a href="{{route('volunteer')}}" >@if(! $lang)Volunteer @else
                                        Կամավոր @endif</a></li>
                            @if(Request::url() == route('index'))
                            <li><a href="#STAFF" class="scroll">@if(! $lang)Staff @elseաշխատակազմը @endif</a></li>
                            @else
                            <li class="@if(Request::url() == route('staff')) active @endif"><a href="{{route('staff')}}">@if(! $lang)Staff @elseաշխատակազմը @endif</a></li>
                            @endif
                            {{--<li><a href="#testimonials" class="scroll">@if(! $lang)Volunteers @else--}}
                                        {{--լուսանկարներ @endif</a></li>--}}
                            <li class="@if(Request::url() == route('donate_us')) active @endif"><a href="{{route('donate_us')}}" >@if(! $lang)Donate @elseՆվիրեք @endif</a></li>
                            <li class="@if(Request::url() == route('contact')) active @endif"><a href="{{route('contact')}}" >@if(! $lang)Contact us @elseԿապ @endif</a></li>
                        </ul>
                    </nav>
                </div>
            </nav>
        </div>
        <div class="clearfix"></div>
    </div>
    <!-- Slideshow 4 -->
    <div class="slider">
        <div class="callbacks_container">
            <ul class="rslides" id="slider4">
                @foreach($slider_texts as $slider_text)
                    <li>
                        <div class="slider-img">
                            <div class="container">
                                <div class="slider-info">
                                    <h4>
                                        @if(! $lang)
                                            {!! $slider_text->big_text !!}
                                        @else
                                            {!! $slider_text->big_text_am !!}
                                        @endif
                                    </h4>
                                    <p>
                                        @if(! $lang)
                                            {!! $slider_text->small_text !!}
                                        @else
                                            {!! $slider_text->small_text_am !!}
                                        @endif
                                    </p>
                                    {{--<div class="outs_more-buttn">--}}
                                    {{--<a href="#" data-toggle="modal" data-target="#myModal">Learn More</a>--}}
                                    {{--</div>--}}
                                </div>
                            </div>
                        </div>
                    </li>
                @endforeach
                {{--<li>--}}
                {{--<div class="slider-img ">--}}
                {{--<div class="container">--}}
                {{--<div class="slider-info">--}}
                {{--<h4>Small Help For Children</h4>--}}
                {{--<p>Quis autem vel eum iure reprehderit.Quis autem vel eum iure reprehderit.</p>--}}
                {{--<div class="outs_more-buttn">--}}
                {{--<a href="#" data-toggle="modal" data-target="#myModal">Learn More</a>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</li>--}}
            </ul>
        </div>
        <div class="clearfix"></div>
    </div>
    <!-- This is here just to demonstrate the callbacks -->
    <!-- <ul class="events">
       <li>Example 4 callback events</li>
       </ul>-->
</div>