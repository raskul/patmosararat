<div class="at-column col-md-3 col-sm-6 col-xs-6">
    <div class="at-user">
        <div class="at-user__avatar"><img src="{{route('index')}}/images/ss4.jpg" alt="img"></div>
        <h4>Ruzanna Shahinyan</h4>
        <div class="at-user__title">
            <h6>Director/Special educator</h6>
        </div>
        <ul class="at-social">
            {{--<li class="at-social__item"><a href="">--}}
            {{--<span class="fa fa-facebook" aria-hidden="true"></span></a>--}}
            {{--</li>--}}
            {{--<li class="at-social__item"><a href="">--}}
            {{--<span class="fa fa-twitter" aria-hidden="true"></span></a>--}}
            {{--</li>--}}
            {{--<li class="at-social__item"><a href="">--}}
            {{--<span class="fa fa-dribbble" aria-hidden="true"></span></a>--}}
            {{--</li>--}}
        </ul>
    </div>
</div>
<div class="at-column col-md-3 col-sm-6 col-xs-6">
    <div class="at-user">
        <div class="at-user__avatar"><img src="{{route('index')}}/images/ss5.jpg" alt="img"></div>
        <h4>Karine Khachatryan</h4>
        <div class="at-user__title">
            <h6>Special Educator</h6>
        </div>
        <ul class="at-social">
            {{--<li class="at-social__item"><a href="">--}}
            {{--<span class="fa fa-facebook" aria-hidden="true"></span></a>--}}
            {{--</li>--}}
            {{--<li class="at-social__item"><a href="">--}}
            {{--<span class="fa fa-twitter" aria-hidden="true"></span></a>--}}
            {{--</li>--}}
            {{--<li class="at-social__item"><a href="">--}}
            {{--<span class="fa fa-dribbble" aria-hidden="true"></span></a>--}}
            {{--</li>--}}
        </ul>
    </div>
</div>
<div class="at-column col-md-3 col-sm-6 col-xs-6">
    <div class="at-user">
        <div class="at-user__avatar"><img src="{{route('index')}}/images/ss3.jpg" alt="img"></div>
        <h4>Hayk Badalyan</h4>
        <div class="at-user__title">
            <h6>Musical therapist</h6>
        </div>
        <ul class="at-social">
            <li class="at-social__item"><a href="https://www.facebook.com/alik.badalyan.14">
                <span class="fa fa-facebook" aria-hidden="true"></span></a>
            </li>
        </ul>
    </div>
</div>
<div class="at-column col-md-3 col-sm-6 col-xs-6">
    <div class="at-user">
        <div class="at-user__avatar"><img src="{{route('index')}}/images/ss1.jpg" alt="img"></div>
        <h4>Carmela Benavides, RN</h4>
        <div class="at-user__title">
            <h6>Medical Nurse / Nurse Coordinator</h6>
        </div>
        <ul class="at-social">
            <li class="at-social__item"><a href="mailto:info@patmosararat.com">
                    <span class="fa fa-envelope" aria-hidden="true"></span></a>
            </li>
        </ul>
    </div>
</div>
<div class="at-column col-md-3 col-sm-6 col-xs-6">
    <div class="at-user">
        <div class="at-user__avatar"><img src="{{route('index')}}/images/ss2.jpg" alt="img"></div>
        <h4>Joanne Khristine Garcia Canubas</h4>
        <div class="at-user__title">
            <h6>Activity Organizer / coordinator</h6>
        </div>
        <ul class="at-social">
            <li class="at-social__item"><a href="mailto:joannecanubas@gmail.com">
                    <span class="fa fa-envelope" aria-hidden="true"></span></a>
            </li>
        </ul>
    </div>
</div>
<div class="at-column col-md-3 col-sm-6 col-xs-6">
    <div class="at-user">
        <div class="at-user__avatar">
            <img src="{{route('index')}}/images/ss6.jpg" alt="img">
        </div>
        <h4>Sadegh Shahbazi</h4>
        <div class="at-user__title">
            <h6>Programmer</h6>
        </div>
        <ul class="at-social">
            <li class="at-social__item"><a href="https://www.facebook.com/sadegh.shahbazi">
                    <span class="fa fa-facebook" aria-hidden="true"></span></a>
            </li>
            <li class="at-social__item"><a href="mailto:sadeghsadegh21@gmail.com">
                    <span class="fa fa-envelope" aria-hidden="true"></span></a>
            </li>
        </ul>
    </div>
</div>
<div class="at-column col-md-3 col-sm-6 col-xs-6">
    <div class="at-user">
        <div class="at-user__avatar">
            <img src="{{route('index')}}/images/ss7.jpg" alt="img">
        </div>
        <h4>Gohar Hovsepyan</h4>
        <div class="at-user__title">
            <h6>Translator</h6>
        </div>
        <ul class="at-social">
            <li class="at-social__item"><a href="mailto:h-gohar@mail.ru">
                    <span class="fa fa-envelope" aria-hidden="true"></span></a>
            </li>
        </ul>
    </div>
</div>