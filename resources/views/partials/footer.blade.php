<div class="footer">
    <div class="container">
        <div class="col-md-5 left-side">
            <h2><a href="{{route('index')}}">@if(! $lang) Patmos-Ararat @else Պատմոս-Արարատ @endif</a></h2>
            <span class="cap"@if(! $lang)> Charity Foundation @else մանկատուն @endif</span>
            {{--<div class="bottom-copy-wrt" style="color: white">--}}
                {{--Nor-Khardberd Orphanage, Bagramyan Street 58,Nor Kharberd Ararat Region, Armenia 0817--}}
            {{--</div>--}}
            {{--<div class="icons">--}}
            {{--<ul>--}}
            {{--<li><a href="#"><span class="fa fa-facebook"></span></a></li>--}}
            {{--<li><a href="#"><span class="fa fa-twitter"></span></a></li>--}}
            {{--<li><a href="#"><span class="fa fa-rss"></span></a></li>--}}
            {{--<li><a href="#"><span class="fa fa-vk"></span></a></li>--}}
            {{--</ul>--}}
            {{--</div>--}}
        </div>
        <div class="col-md-7 right-side">
        {{--<nav>--}}
        {{--<ul class="nav-buttom">--}}
        {{--<li><a class="page-scroll scroll" href="#home">Home</a></li>--}}
        {{--<li><a class="page-scroll scroll" href="#about">About</a></li>--}}
        {{--<li><a class="page-scroll scroll" href="#services">Services</a></li>--}}
        {{--<li><a class="page-scroll scroll" href="#Events">Events</a></li>--}}
        {{--<li><a class="page-scroll scroll" href="#testimonials">Donors</a></li>--}}
        {{--<li><a class="page-scroll scroll" href="#contact">Contact</a></li>--}}
        {{--</ul>--}}
        {{--</nav>--}}
        <div class="bottom-copy-wrt">
            @inject('footer','App\classes\helpers\footer')
        <p>
            @if(! $lang)
                {!! $footer->getFooter()->text !!}
            @else
                {!! $footer->getFooter()->text_am !!}
            @endif
        </p>
        </div>
        </div>
    </div>
</div>