<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $volunteer->id !!}</p>
</div>

<!-- Text Field -->
<div class="form-group">
    {!! Form::label('text', 'Text:') !!}
    <p>{!! $volunteer->text !!}</p>
</div>

<!-- Text Am Field -->
<div class="form-group">
    {!! Form::label('text_am', 'Text Am:') !!}
    <p>{!! $volunteer->text_am !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $volunteer->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $volunteer->updated_at !!}</p>
</div>

