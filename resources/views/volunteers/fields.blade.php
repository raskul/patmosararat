area<!-- Text Field -->
<div class="form-group col-sm-6">
    {!! Form::label('text', 'Text:') !!}
    {!! Form::textarea('text', null, ['class' => 'form-control']) !!}
</div>

<!-- Text Am Field -->
<div class="form-group col-sm-6">
    {!! Form::label('text_am', 'Text Am:') !!}
    {!! Form::textarea('text_am', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('volunteers.index') !!}" class="btn btn-default">Cancel</a>
</div>
