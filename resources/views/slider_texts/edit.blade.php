@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Slider Text
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($sliderText, ['route' => ['sliderTexts.update', $sliderText->id], 'method' => 'patch']) !!}

                        @include('slider_texts.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection