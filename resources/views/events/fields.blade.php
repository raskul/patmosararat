<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('time', 'Time:') !!}
    <div id="picker"></div>
    <input type="hidden" id="result" value="2017-01-01 00:00" name="time">
    {{--    {!! Form::text('time', null, ['class' => 'form-control', 'type' => 'hidden', 'id' => 'result']) !!}--}}
</div>

<!-- Image Field -->
<div class="form-group col-sm-6">
    {!! Form::label('image', 'Image:') !!}
    {!! Form::file('image', null, ['class' => 'form-control']) !!}
</div>

@if(isset($event->image))
    <div class="form-group col-sm-6">
        <img src="{{route('index')}}/uploads/events/image/{{$event->image}}">
    </div>
@endif

<!-- Image Thumbnail Field -->
{{--<div class="form-group col-sm-6">--}}
{{--    {!! Form::label('image_thumbnail', 'Image Thumbnail:') !!}--}}
    {!! Form::hidden('image_thumbnail', null, ['class' => 'form-control']) !!}
{{--</div>--}}

<!-- Time Field -->



<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('events.index') !!}" class="btn btn-default">Cancel</a>
</div>
