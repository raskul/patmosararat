<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $photoGallery->id !!}</p>
</div>

<!-- Event Id Field -->
<div class="form-group">
    {!! Form::label('event_id', 'Event Id:') !!}
    <p>{!! $photoGallery->event_id !!}</p>
</div>

<!-- Image Field -->
<div class="form-group">
    {!! Form::label('image', 'Image:') !!}
    <p>{!! $photoGallery->image !!}</p>
</div>

<!-- Image Thumbnail Field -->
<div class="form-group">
    {!! Form::label('image_thumbnail', 'Image Thumbnail:') !!}
    <p>{!! $photoGallery->image_thumbnail !!}</p>
</div>

<!-- Who Create Field -->
<div class="form-group">
    {!! Form::label('who_create', 'Who Create:') !!}
    <p>{!! $photoGallery->who_create !!}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{!! $photoGallery->description !!}</p>
</div>

<!-- Type Field -->
<div class="form-group">
    {!! Form::label('type', 'Type:') !!}
    <p>{!! $photoGallery->type !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $photoGallery->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $photoGallery->updated_at !!}</p>
</div>

