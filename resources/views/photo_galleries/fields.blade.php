<!-- Event Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('event_id', 'Event Name:') !!}
    {{--{!! Form::text('event_id', null, ['class' => 'form-control']) !!}--}}
    <select name="event_id" id="event_id" class="form-control">
        @foreach($events as $event)
            <option value="{{$event->id}}">{{$event->name}}</option>
        @endforeach
    </select>
</div>

<div class="form-group col-sm-6">
    {!! Form::label('description', 'Description:') !!}
    {!! Form::text('description', null, ['class' => 'form-control']) !!}
</div>

<!-- Image Field -->
<div class="form-group col-sm-6">
    {!! Form::label('image', 'Image:') !!}
    {!! Form::file('image', null, ['class' => 'form-control']) !!}
</div>

<!-- Image Thumbnail Field -->
{{--<div class="form-group col-sm-6">--}}
    {{--{!! Form::label('image_thumbnail', 'Image Thumbnail:') !!}--}}
    {!! Form::hidden('image_thumbnail', null, ['class' => 'form-control']) !!}
{{--</div>--}}

<!-- Who Create Field -->
{{--<div class="form-group col-sm-6">--}}
    {{--{!! Form::label('who_create', 'Who Create:') !!}--}}
    {!! Form::hidden('who_create', null, ['class' => 'form-control']) !!}
{{--</div>--}}

<!-- Description Field -->

<!-- Type Field -->
{{--<div class="form-group col-sm-6">--}}
    {{--{!! Form::label('type', 'Type:') !!}--}}
    {!! Form::hidden('type', null, ['class' => 'form-control']) !!}
{{--</div>--}}

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('photoGalleries.index') !!}" class="btn btn-default">Cancel</a>
</div>
