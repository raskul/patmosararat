@extends('layouts.app')
@push('header')
    <style>
        .small-box {
            border-radius: 2px;
            position: relative;
            display: block;
            margin-bottom: 20px;
            box-shadow: 0 1px 1px rgba(0, 0, 0, 0.1);
        }

        .small-box > .inner {
            padding: 10px;
        }

        .small-box .icon {
            -webkit-transition: all .3s linear;
            -o-transition: all .3s linear;
            transition: all .3s linear;
            position: absolute;
            top: -10px;
            left: 10px;
            z-index: 0;
            font-size: 90px;
            color: rgba(0, 0, 0, 0.15);
        }

        .small-box > .small-box-footer {
            position: relative;
            text-align: center;
            padding: 3px 0;
            color: #fff;
            color: rgba(255, 255, 255, 0.8);
            display: block;
            z-index: 10;
            background: rgba(0, 0, 0, 0.1);
            text-decoration: none;
        }

        .bg-aqua {
            background-color: #00c0ef !important;
        }

        .bg-green {
            background-color: #00a65a !important;
        }

        .bg-yellow {
            background-color: #f39c12 !important;
        }

        .bg-red {
            background-color: #dd4b39 !important;
        }

        .padding-50-all {
            padding: 50px;
        }
    </style>

    <style>
        .fa{
            font-size: .8em
        }
    </style>
@endpush
@section('content')
    <div class="row ">
        <div class="row " >
            <div class="col-md-3 "  style="padding-left: 30px" >
                <!-- sma ll box -->
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3>{{$request_count}}</h3>
                        <p>volunteer new request</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-envelope"></i>
                    </div>
                    <a href="{{route('volunteerRequest')}}" class="small-box-footer"> read more <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            {{--<div class="col-lg-3 col-xs-6">--}}
                {{--<!-- small box -->--}}
                {{--<div class="small-box bg-green">--}}
                    {{--<div class="inner">--}}
                        {{--<h3>15</h3>--}}
                        {{--<p>تیکت های بسته نشده</p>--}}
                    {{--</div>--}}
                    {{--<div class="icon">--}}
                        {{--<i class="fa fa-envelope-open"></i>--}}
                    {{--</div>--}}
                    {{--<a href="{{route('index')}}" class="small-box-footer">اطلاعات بیشتر <i--}}
                                {{--class="fa fa-arrow-circle-left"></i></a>--}}
                {{--</div>--}}
            {{--</div>--}}
            <!-- ./col -->
            {{--<div class="col-lg-3 col-xs-6">--}}
                {{--<!-- small box -->--}}
                {{--<div class="small-box bg-yellow">--}}
                    {{--<div class="inner">--}}
                        {{--<h3>{{$teaching_request}}</h3>--}}

                        {{--<p>درخواست های تدریس معلق</p>--}}
                    {{--</div>--}}
                    {{--<div class="icon">--}}
                        {{--<i class="ionicons ion-person-stalker"></i>--}}
                    {{--</div>--}}
                    {{--<a href="{{route('teachingIndex')}}" class="small-box-footer">اطلاعات بیشتر <i--}}
                                {{--class="fa fa-arrow-circle-left"></i></a>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<!-- ./col -->--}}
            {{--<div class="col-lg-3 col-xs-6">--}}
                {{--<!-- small box -->--}}
                {{--<div class="small-box bg-red">--}}
                    {{--<div class="inner">--}}
                        {{--<h3>{{$factor_by_post_not_send}}</h3>--}}

                        {{--<p>فاکتور های پستی ارسال نشده</p>--}}
                    {{--</div>--}}
                    {{--<div class="icon">--}}
                        {{--<i class="fa fa-share-square"></i>--}}
                    {{--</div>--}}
                    {{--<a href="{{route('factors.index')}}" class="small-box-footer">اطلاعات بیشتر <i--}}
                                {{--class="fa fa-arrow-circle-left"></i></a>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<!-- ./col -->--}}
            {{--<div class="col-lg-3 col-xs-6">--}}
                {{--<!-- small box -->--}}
                {{--<div class="small-box bg-aqua">--}}
                    {{--<div class="inner">--}}
                        {{--<h3>{{$withdraw_request}}</h3>--}}
                        {{--<p>درخواست های برداشت تایید نشده</p>--}}
                    {{--</div>--}}
                    {{--<div class="icon">--}}
                        {{--<i class="fa fa-money"></i>--}}
                    {{--</div>--}}
                    {{--<a href="{{route('adminFinancialWithdraw')}}" class="small-box-footer">اطلاعات بیشتر <i class="fa fa-arrow-circle-left"></i></a>--}}
                {{--</div>--}}
            {{--</div>--}}
            <!-- ./col -->
        </div>

    </div>

<div class="container">
    <div class="row">


    </div>
</div>
@endsection
