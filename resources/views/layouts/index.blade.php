<!DOCTYPE html>
<html lang="EN">
<head>

    <title>Patmosararat</title>
    <!--meta tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script>
        addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>
    <!--//meta tags ends here-->
    <!--booststrap-->
    <link href="{{asset('css/bootstrap.css')}}" rel="stylesheet" type="text/css" media="all">
    <!--//booststrap end-->
    <!-- font-awesome icons -->
    <link href="{{asset('css/font-awesome.css')}}" rel="stylesheet">
    <!-- //font-awesome icons -->
    <link rel="stylesheet" href="{{asset('css/owl.carousel.css')}}" type="text/css" media="all">
    <!-- clients-->
    <!--stylesheets-->
    <link href="{{asset('css/style.css')}}" rel='stylesheet' type='text/css' media="all">
    <!--//stylesheets-->
    <link href="//fonts.googleapis.com/css?family=Montserrat:300,400,500" rel="stylesheet">


</head>

@yield('header')

@stack('header')

<body>

@yield('content')


@yield('scripts')
@stack('scripts')

<!--footer-->
<!--js working-->
<script src='{{asset('js/jquery-2.2.3.min.js')}}'></script>
<!-- //js  working-->
<script src="{{asset('js/responsiveslides.min.js')}}"></script>
<script>
    // You can also use "$(window).load(function() {"
    $(function () {
        // Slideshow 4
        $("#slider4").responsiveSlides({
            auto: true,
            pager: false,
            nav: true,
            speed: 900,
            namespace: "callbacks",
            before: function () {
                $('.events').append("<li>before event fired.</li>");
            },
            after: function () {
                $('.events').append("<li>after event fired.</li>");
            }
        });

    });
</script>
<!--// banner-->
<!--client carousel -->
<script src="{{asset('js/owl.carousel.js')}}"></script>
<script>
    $(document).ready(function () {
        $("#owl-demo").owlCarousel({
            items: 1,
            itemsDesktop: [768, 1],
            itemsDesktopSmall: [414, 1],
            lazyLoad: true,
            autoPlay: true,
            navigation: true,

            navigationText: false,
            pagination: true,

        });

    });
</script>
<!-- start-smoth-scrolling -->
<script  src="{{asset('js/move-top.js')}}"></script>
<script  src="{{asset('js/easing.js')}}"></script>
<script >
    jQuery(document).ready(function ($) {
        $(".scroll").click(function (event) {
            event.preventDefault();
            $('html,body').animate({ scrollTop: $(this.hash).offset().top }, 1000);
        });
    });
</script>
<!-- start-smoth-scrolling -->
<!-- here stars scrolling icon -->
<script>
    $(document).ready(function () {
        /*
            var defaults = {
            containerID: 'toTop', // fading element id
            containerHoverID: 'toTopHover', // fading element hover id
            scrollSpeed: 1200,
            easingType: 'linear'
            };
        */

        $().UItoTop({ easingType: 'easeOutQuart' });

    });
</script>
<!-- //here ends scrolling icon -->
<!--bootstrap working-->
<script src="{{asset('js/bootstrap.js')}}"></script>
<!-- //bootstrap working-->

</body>
</html>