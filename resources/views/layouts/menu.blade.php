<li>
    <a href="{!! route('index') !!}"><i class="fa fa-desktop"></i><span>Website</span></a>
</li>
<li class="{{ Request::is('home') ? 'active' : '' }}">
    <a href="{!! route('index') !!}/home"><i class="fa fa-home"></i><span>Home</span></a>
</li>
<hr style="border-color: #2c3b41">
@can('is-admin')
<li class="{{ Request::is('sliderTexts*') ? 'active' : '' }}">
    <a href="{!! route('sliderTexts.index') !!}"><i class="fa fa-edit"></i><span>Slider Texts</span></a>
</li>

<li class="{{ Request::is('aboutuses*') ? 'active' : '' }}">
    <a href="{!! route('aboutuses.index') !!}"><i class="fa fa-edit"></i><span>About us Text</span></a>
</li>

<li class="{{ Request::is('contacts*') ? 'active' : '' }}">
    <a href="{!! route('contacts.index') !!}"><i class="fa fa-edit"></i><span>Contact us Text</span></a>
</li>

{{--<li class="{{ Request::is('services*') ? 'active' : '' }}">--}}
    {{--<a href="{!! route('services.index') !!}"><i class="fa fa-edit"></i><span>Services</span></a>--}}
{{--</li>--}}

<li class="{{ Request::is('activities*') ? 'active' : '' }}">
    <a href="{!! route('activities.index') !!}"><i class="fa fa-edit"></i><span>Activities Text</span></a>
</li>

<li class="{{ Request::is('donates*') ? 'active' : '' }}">
    <a href="{!! route('donates.index') !!}"><i class="fa fa-edit"></i><span>Donates Text</span></a>
</li>

<li class="{{ Request::is('volunteers*') ? 'active' : '' }}">
    <a href="{!! route('volunteers.index') !!}"><i class="fa fa-edit"></i><span>Volunteer Text</span></a>
</li>

<li class="{{ Request::is('footers*') ? 'active' : '' }}">
    <a href="{!! route('footers.index') !!}"><i class="fa fa-edit"></i><span>Footers Text</span></a>
</li>

<hr style="border-color: #2c3b41">

<li class="{{ Request::is('users*') ? 'active' : '' }}">
    <a href="{!! route('users.index') !!}"><i class="fa fa-user"></i><span>Users</span></a>
</li>

@endcan

<hr style="border-color: #2c3b41">

@can('is-editor')
<li class="{{ Request::is('events*') ? 'active' : '' }}">
    <a href="{!! route('events.index') !!}"><i class="fa fa-calendar"></i><span>Events</span></a>
</li>

<li class="{{ Request::is('photoGalleries*') ? 'active' : '' }}">
    <a href="{!! route('photoGalleries.index') !!}"><i class="fa fa-camera"></i><span>Photo Galleries</span></a>
</li>
@endcan




