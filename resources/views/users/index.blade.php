@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Users</h1>
        <h1 class="pull-right">
            <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px"
               href="{!! route('users.create') !!}">Add New</a>
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">

                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>Email</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                        <tr  style="background-color:
                        @if($user->role_id == 0) rgba(255,0,0,0.5) @endif
                        @if($user->role_id == 1) rgba(255,242,247,0.57) @endif
                        @if($user->role_id == 2) rgba(255,212,26,0.57) @endif
                        @if($user->role_id == 3) rgba(92,255,99,0.57) @endif
                        @if($user->role_id == 4) rgba(87,99,255,0.57) @endif
                        @if($user->role_id == 5) rgba(0,35,255,0.57) @endif

                                "  >
                            <td>{{$user->email}}</td>
                            <td>
                                {!! Form::open(['route'=>['users.destroy', $user->id ], 'method' => 'delete' ]) !!}
                                <table>
                                    <tr>
                                        <td>
                                            <div class="btn-group">
                                                {{--<a href="{{ route('users.show', $user->id) }}"--}}
                                                   {{--class='btn btn-default btn-xs'>--}}
                                                    {{--<i class="glyphicon glyphicon-eye-open"></i>--}}
                                                {{--</a>--}}
                                                <a href="{{ route('users.edit', $user->id) }}"
                                                   class='btn btn-default btn-xs'>
                                                    <i class="glyphicon glyphicon-edit"></i>
                                                </a>
                                                {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', [
                                                        'type' => 'submit',
                                                        'class' => 'btn btn-danger btn-xs',
                                                        'onclick' => "return confirm('Are you sure?')"
                                                    ]) !!}
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

                {{$users->links()}}

            </div>


        </div>
        <span style="background-color: rgba(255,242,247,0.57)">normal user</span><br>
        <span style="background-color: rgba(255,0,0,0.5)">rejected volunteer</span><br>
        <span style="background-color: rgba(255,212,26,0.57)">requested volunteer</span><br>
        <span style="background-color: rgba(92,255,99,0.57)">accepted volunteer</span><br>
        <span style="background-color: rgba(87,99,255,0.57)">editor</span><br>
        <span style="background-color: rgba(0,35,255,0.57)">manager</span><br>

        <div class="text-center">

        </div>
    </div>
@endsection

