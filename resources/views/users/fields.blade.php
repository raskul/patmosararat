<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group col-sm-6">
    {!! Form::label('last_name', 'last name:') !!}
    {!! Form::text('last_name', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group col-sm-6">
    {!! Form::label('email', 'email:') !!}
    {!! Form::text('email', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('first_name', 'real email:') !!}
    {!! Form::text('first_name', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('role_id', 'role:') !!}
    <select name="role_id" class="form-control" id="">
        <option value="1" @if((isset($user))&&($user->role_id == 1)) selected @endif >Normal user</option>
        <option value="0" @if((isset($user))&&($user->role_id == 0)) selected @endif >Rejected volunteer</option>
        <option value="2" @if((isset($user))&&($user->role_id == 2)) selected @endif >Requested volunteer</option>
        <option value="3" @if((isset($user))&&($user->role_id == 3)) selected @endif >volunteer</option>
        <option value="4" @if((isset($user))&&($user->role_id == 4)) selected @endif >Editor</option>
        <option value="5" @if((isset($user))&&($user->role_id == 5)) selected @endif >Manager</option>
    </select>
</div>

<div class="form-group col-sm-6">
    {!! Form::label('image', 'image:') !!}
    {!! Form::file('image', ['class' => 'form-control']) !!}
</div>

@if(isset($user->image))
    <div class="form-group col-sm-6">
        {!! Form::label('now_image', 'uploaded image:') !!}
        <img width="100%" src="{{route('index')}}/uploads/users/image/{{$user->image}}">
    </div>
@endif

<div class="form-group col-sm-6">
    {!! Form::label('country', 'country:') !!}
    {!! Form::text('country', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('address', 'address:') !!}
    {!! Form::text('address', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('profession', 'profession:') !!}
    {!! Form::text('profession', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('age', 'facebook:') !!}
    {!! Form::text('age', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('mobile', 'mobile:') !!}
    {!! Form::text('mobile', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('about_me', 'about me:') !!}
    {!! Form::text('about_me', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('start_activity', 'start volunteer date:(you can leave empty)') !!}
    {!! Form::text('start_activity', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('end_activity', 'end volunteer activity:(you can leave empty)') !!}
    {!! Form::text('end_activity', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('birthday', 'birthday:') !!}
    <select name="birthday" id="" class="form-control">
        @for($i = 0; $i <120; $i++  )
        <option value="{{$i+1900}}" @if((isset($user)) && ($user->birthday == $i+1900) ) selected @endif >{{$i+1900}}</option>
        @endfor
    </select>
</div>

<div class="form-group col-sm-6">
    {!! Form::label('gender', 'gender:') !!}
    <select name="gender" class="form-control" id="">
        <option value="male" @if((isset($user))&&($user->gender == 'male')) selected @endif >male</option>
        <option value="female" @if((isset($user))&&($user->gender == 'female')) selected @endif >female</option>
        <option value="other" @if((isset($user))&&($user->gender == 'other')) selected @endif >other</option>
    </select>
</div>



@if((isset($user)) && (isset($user->upload)))
    <div class="form-group col-sm-6">
        <div>
            <a href="{{route('index')}}/{{$user->upload}}">Click here to download CV</a>
        </div>
    </div>
@endif

{{--<div class="form-group col-sm-6">--}}
    {{--{!! Form::label('birthday', 'birthday:') !!}--}}
    {{--{!! Form::text('birthday', null, ['class' => 'form-control']) !!}--}}
{{--</div>--}}

{{--<div class="form-group col-sm-6">--}}
    {{--{!! Form::label('birthday', 'birthday:') !!}--}}
    {{--{!! Form::text('birthday', null, ['class' => 'form-control']) !!}--}}
{{--</div>--}}


{{--<div class="form-group col-sm-6">--}}
    {{--{!! Form::label('time', 'Time:') !!}--}}
    {{--<div id="picker"></div>--}}
    {{--<input type="hidden" id="result" value="2017-01-01 00:00" name="time">--}}
{{--</div>--}}

<!-- Image Field -->
{{--<div class="form-group col-sm-6">--}}
    {{--{!! Form::label('image', 'Image:') !!}--}}
    {{--{!! Form::file('image', null, ['class' => 'form-control']) !!}--}}
{{--</div>--}}


<!-- Image Thumbnail Field -->
{{--<div class="form-group col-sm-6">--}}
{{--    {!! Form::label('image_thumbnail', 'Image Thumbnail:') !!}--}}
{{--    {!! Form::hidden('image_thumbnail', null, ['class' => 'form-control']) !!}--}}
{{--</div>--}}

<!-- Time Field -->



<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('events.index') !!}" class="btn btn-default">Cancel</a>
</div>
