<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $activity->id !!}</p>
</div>

<!-- Left Text Field -->
<div class="form-group">
    {!! Form::label('left_text', 'Left Text:') !!}
    <p>{!! $activity->left_text !!}</p>
</div>

<!-- Right Text Field -->
<div class="form-group">
    {!! Form::label('right_text', 'Right Text:') !!}
    <p>{!! $activity->right_text !!}</p>
</div>

<!-- Left Text Am Field -->
<div class="form-group">
    {!! Form::label('left_text_am', 'Left Text Am:') !!}
    <p>{!! $activity->left_text_am !!}</p>
</div>

<!-- Right Text Am Field -->
<div class="form-group">
    {!! Form::label('right_text_am', 'Right Text Am:') !!}
    <p>{!! $activity->right_text_am !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $activity->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $activity->updated_at !!}</p>
</div>

