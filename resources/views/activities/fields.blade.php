<!-- Left Text Field -->
<div class="form-group col-sm-6">
    {!! Form::label('left_text', 'Left Text:') !!}
    {!! Form::textarea('left_text', null, ['class' => 'form-control']) !!}
</div>

<!-- Right Text Field -->
<div class="form-group col-sm-6">
    {!! Form::label('right_text', 'Right Text:') !!}
    {!! Form::textarea('right_text', null, ['class' => 'form-control']) !!}
</div>

<!-- Left Text Am Field -->
<div class="form-group col-sm-6">
    {!! Form::label('left_text_am', 'Left Text Am:') !!}
    {!! Form::textarea('left_text_am', null, ['class' => 'form-control']) !!}
</div>

<!-- Right Text Am Field -->
<div class="form-group col-sm-6">
    {!! Form::label('right_text_am', 'Right Text Am:') !!}
    {!! Form::textarea('right_text_am', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('activities.index') !!}" class="btn btn-default">Cancel</a>
</div>
