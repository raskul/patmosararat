<!-- Big Text Field -->
<div class="form-group col-sm-6">
    {!! Form::label('big_text', 'Big Text:') !!}
    {!! Form::text('big_text', null, ['class' => 'form-control']) !!}
</div>

<!-- Small Text Field -->
<div class="form-group col-sm-6">
    {!! Form::label('small_text', 'Small Text:') !!}
    {!! Form::text('small_text', null, ['class' => 'form-control']) !!}
</div>

<!-- Big Text Am Field -->
<div class="form-group col-sm-6">
    {!! Form::label('big_text_am', 'Big Text Am:') !!}
    {!! Form::text('big_text_am', null, ['class' => 'form-control']) !!}
</div>

<!-- Small Text Am Field -->
<div class="form-group col-sm-6">
    {!! Form::label('small_text_am', 'Small Text Am:') !!}
    {!! Form::text('small_text_am', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('services.index') !!}" class="btn btn-default">Cancel</a>
</div>
