<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $service->id !!}</p>
</div>

<!-- Big Text Field -->
<div class="form-group">
    {!! Form::label('big_text', 'Big Text:') !!}
    <p>{!! $service->big_text !!}</p>
</div>

<!-- Small Text Field -->
<div class="form-group">
    {!! Form::label('small_text', 'Small Text:') !!}
    <p>{!! $service->small_text !!}</p>
</div>

<!-- Big Text Am Field -->
<div class="form-group">
    {!! Form::label('big_text_am', 'Big Text Am:') !!}
    <p>{!! $service->big_text_am !!}</p>
</div>

<!-- Small Text Am Field -->
<div class="form-group">
    {!! Form::label('small_text_am', 'Small Text Am:') !!}
    <p>{!! $service->small_text_am !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $service->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $service->updated_at !!}</p>
</div>

