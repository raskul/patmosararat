<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $aboutUs->id !!}</p>
</div>

<!-- Text Field -->
<div class="form-group">
    {!! Form::label('text', 'Text:') !!}
    <p>{!! $aboutUs->text !!}</p>
</div>

<!-- Text Am Field -->
<div class="form-group">
    {!! Form::label('text_am', 'Text Am:') !!}
    <p>{!! $aboutUs->text_am !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $aboutUs->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $aboutUs->updated_at !!}</p>
</div>

