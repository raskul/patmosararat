@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            About Us
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($aboutUs, ['route' => ['aboutuses.update', $aboutUs->id], 'method' => 'patch']) !!}

                        @include('aboutuses.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection