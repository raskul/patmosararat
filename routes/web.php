<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//if (env('APP_ENV') === 'production') {
//    dd('hi');
//    URL::forceSchema('https');
//}



Route::get('/', 'IndexController@index')->name('index');
//Route::get('/en', 'IndexController@index_en')->name('index_en');
//Route::get('/am', 'IndexController@index_am')->name('index_am');

Route::get('/lang_en', 'IndexController@lang_en')->name('lang_en');
Route::get('/lang_am', 'IndexController@lang_am')->name('lang_am');

Route::get('about_us', 'IndexController@about_us')->name('about_us');
Route::get('contact', 'IndexController@contact')->name('contact');
Route::get('staff', 'IndexController@staff')->name('staff');
Route::get('activity', 'IndexController@activities')->name('activities');
Route::get('volunteer', 'IndexController@volunteer')->name('volunteer');
Route::match(['GET', 'POST'],'volunteer-apply', 'IndexController@volunteerApply')->name('volunteerApply');
Route::get('donate', 'IndexController@donate')->name('donate_us');
Route::get('photo_gallery/{event?}', 'IndexController@photoGallery')->name('photo_gallery');
Route::get('volunteer-request', 'HomeController@volunteerRequest')->name('volunteerRequest');

//Route::get('c', function(){

//    $l = $request->cookies->has('lang');
//    Cookie::queue(Cookie::forget('lang'));
//    Cookie::queue(Cookie::forget('lang'));
//    Cookie::unqueue('lang');
//    Cookie::unqueue(Cookie::forever('lang'));
//    dd(Cookie::get('lang'));
//    return view('index.index');
//});

//Route::get('d', function(){
//    dd(Cookie::get('lang'));
//});

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::resource('sliderTexts', 'SliderTextController');

Route::resource('aboutuses', 'AboutUsController');

Route::resource('contacts', 'ContactController');

Route::resource('services', 'ServiceController');

Route::resource('activities', 'ActivityController');

Route::resource('events', 'EventController');

Route::resource('donates', 'DonateController');

Route::resource('photoGalleries', 'PhotoGalleryController');

Route::resource('volunteers', 'VolunteerController');

Route::resource('footers', 'FooterController');

Route::resource('users', 'UserController');