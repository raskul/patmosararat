<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('is-user', function ($user) {//role id = 1
            if (($user->role_id == 1) || ($user->role_id == 2) || ($user->role_id == 3) || ($user->role_id == 4) || ($user->role_id == 5)) {
                return true;
            }
        });

        Gate::define('is-volunteer', function ($user) {//role id = 2
            if (($user->role_id == 2) || ($user->role_id == 3) || ($user->role_id == 4) || ($user->role_id == 5)) {
                return true;
            }
        });

        Gate::define('is-writer', function ($user) {//role id = 3
            if (($user->role_id == 3) || ($user->role_id == 4) || ($user->role_id == 5)) {
                return true;
            }
        });

        Gate::define('is-editor', function ($user) {//role id = 4
            if (($user->role_id == 4) || ($user->role_id == 5)) {
                return true;
            }
        });

        Gate::define('is-admin', function ($user) {//role id = 5
            if (($user->role_id == 5)) {
                return true;
            }
        });

    }
}
