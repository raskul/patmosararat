<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'role_id',
        'password',
        'first_name',
        'last_name',
        'birthday',
        'mobile',
        'gender',
        'image',
        'country',
        'address',
        'profession',
        'age',
        'about_me',
        'start_activity',
        'end_activity',
        'first_name',
        'gender',
        'upload',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
