<?php

namespace App\Http\Controllers;

use App\DataTables\DonateDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateDonateRequest;
use App\Http\Requests\UpdateDonateRequest;
use App\Repositories\DonateRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class DonateController extends AppBaseController
{
    /** @var  DonateRepository */
    private $donateRepository;

    public function __construct(DonateRepository $donateRepo)
    {
        $this->donateRepository = $donateRepo;
    }

    /**
     * Display a listing of the Donate.
     *
     * @param DonateDataTable $donateDataTable
     * @return Response
     */
    public function index(DonateDataTable $donateDataTable)
    {
        $this->authorize('is-admin');

        return $donateDataTable->render('donates.index');
    }

    /**
     * Show the form for creating a new Donate.
     *
     * @return Response
     */
    public function create()
    {
        $this->authorize('is-admin');

        return view('donates.create');
    }

    /**
     * Store a newly created Donate in storage.
     *
     * @param CreateDonateRequest $request
     *
     * @return Response
     */
    public function store(CreateDonateRequest $request)
    {
        $this->authorize('is-admin');

        $input = $request->all();

        $donate = $this->donateRepository->create($input);

        Flash::success('Donate saved successfully.');

        return redirect(route('donates.index'));
    }

    /**
     * Display the specified Donate.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $this->authorize('is-admin');

        $donate = $this->donateRepository->findWithoutFail($id);

        if (empty($donate)) {
            Flash::error('Donate not found');

            return redirect(route('donates.index'));
        }

        return view('donates.show')->with('donate', $donate);
    }

    /**
     * Show the form for editing the specified Donate.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $this->authorize('is-admin');

        $donate = $this->donateRepository->findWithoutFail($id);

        if (empty($donate)) {
            Flash::error('Donate not found');

            return redirect(route('donates.index'));
        }

        return view('donates.edit')->with('donate', $donate);
    }

    /**
     * Update the specified Donate in storage.
     *
     * @param  int              $id
     * @param UpdateDonateRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDonateRequest $request)
    {
        $this->authorize('is-admin');

        $donate = $this->donateRepository->findWithoutFail($id);

        if (empty($donate)) {
            Flash::error('Donate not found');

            return redirect(route('donates.index'));
        }

        $donate = $this->donateRepository->update($request->all(), $id);

        Flash::success('Donate updated successfully.');

        return redirect(route('donates.index'));
    }

    /**
     * Remove the specified Donate from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $this->authorize('is-admin');

        $donate = $this->donateRepository->findWithoutFail($id);

        if (empty($donate)) {
            Flash::error('Donate not found');

            return redirect(route('donates.index'));
        }

        $this->donateRepository->delete($id);

        Flash::success('Donate deleted successfully.');

        return redirect(route('donates.index'));
    }
}
