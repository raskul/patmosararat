<?php

namespace App\Http\Controllers;

use App\DataTables\VolunteerDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateVolunteerRequest;
use App\Http\Requests\UpdateVolunteerRequest;
use App\Repositories\VolunteerRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class VolunteerController extends AppBaseController
{
    /** @var  VolunteerRepository */
    private $volunteerRepository;

    public function __construct(VolunteerRepository $volunteerRepo)
    {
        $this->volunteerRepository = $volunteerRepo;
    }

    /**
     * Display a listing of the Volunteer.
     *
     * @param VolunteerDataTable $volunteerDataTable
     * @return Response
     */
    public function index(VolunteerDataTable $volunteerDataTable)
    {
        $this->authorize('is-admin');

        return $volunteerDataTable->render('volunteers.index');
    }

    /**
     * Show the form for creating a new Volunteer.
     *
     * @return Response
     */
    public function create()
    {
        $this->authorize('is-admin');

        return view('volunteers.create');
    }

    /**
     * Store a newly created Volunteer in storage.
     *
     * @param CreateVolunteerRequest $request
     *
     * @return Response
     */
    public function store(CreateVolunteerRequest $request)
    {
        $this->authorize('is-admin');

        $input = $request->all();

        $volunteer = $this->volunteerRepository->create($input);

        Flash::success('Volunteer saved successfully.');

        return redirect(route('volunteers.index'));
    }

    /**
     * Display the specified Volunteer.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $this->authorize('is-admin');

        $volunteer = $this->volunteerRepository->findWithoutFail($id);

        if (empty($volunteer)) {
            Flash::error('Volunteer not found');

            return redirect(route('volunteers.index'));
        }

        return view('volunteers.show')->with('volunteer', $volunteer);
    }

    /**
     * Show the form for editing the specified Volunteer.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $this->authorize('is-admin');

        $volunteer = $this->volunteerRepository->findWithoutFail($id);

        if (empty($volunteer)) {
            Flash::error('Volunteer not found');

            return redirect(route('volunteers.index'));
        }

        return view('volunteers.edit')->with('volunteer', $volunteer);
    }

    /**
     * Update the specified Volunteer in storage.
     *
     * @param  int              $id
     * @param UpdateVolunteerRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateVolunteerRequest $request)
    {
        $this->authorize('is-admin');

        $volunteer = $this->volunteerRepository->findWithoutFail($id);

        if (empty($volunteer)) {
            Flash::error('Volunteer not found');

            return redirect(route('volunteers.index'));
        }

        $volunteer = $this->volunteerRepository->update($request->all(), $id);

        Flash::success('Volunteer updated successfully.');

        return redirect(route('volunteers.index'));
    }

    /**
     * Remove the specified Volunteer from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $this->authorize('is-admin');

        $volunteer = $this->volunteerRepository->findWithoutFail($id);

        if (empty($volunteer)) {
            Flash::error('Volunteer not found');

            return redirect(route('volunteers.index'));
        }

        $this->volunteerRepository->delete($id);

        Flash::success('Volunteer deleted successfully.');

        return redirect(route('volunteers.index'));
    }
}
