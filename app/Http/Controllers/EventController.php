<?php

namespace App\Http\Controllers;

use App\DataTables\EventDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateEventRequest;
use App\Http\Requests\UpdateEventRequest;
use App\Repositories\EventRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Intervention\Image\Facades\Image;
use Response;

class EventController extends AppBaseController
{
    /** @var  EventRepository */
    private $eventRepository;

    public function __construct(EventRepository $eventRepo)
    {
        $this->eventRepository = $eventRepo;
    }

    /**
     * Display a listing of the Event.
     *
     * @param EventDataTable $eventDataTable
     * @return Response
     */
    public function index(EventDataTable $eventDataTable)
    {
        $this->authorize('is-editor');

        return $eventDataTable->render('events.index');
    }

    /**
     * Show the form for creating a new Event.
     *
     * @return Response
     */
    public function create()
    {
        $this->authorize('is-editor');

        return view('events.create');
    }

    /**
     * Store a newly created Event in storage.
     *
     * @param CreateEventRequest $request
     *
     * @return Response
     */
    public function store(CreateEventRequest $request)
    {
        $this->authorize('is-editor');

        $inputs = $request->all();

        $inputs['image'] = time() . rand(1000, 9999) . $request->file('image')->getClientOriginalName();
        $path = public_path() . '/uploads/events/image/';
        $request->image->move($path, $inputs['image']);
        Image::make($path . $inputs['image'])
            ->resize('500', '300')
            ->save(public_path() . '/uploads/events/image/thumbnail/' . $inputs['image']);
        $inputs['image_thumbnail'] = $inputs['image'];

//        dd($inputs);

        $event = $this->eventRepository->create($inputs);


        Flash::success('Event saved successfully.');

        return redirect(route('events.index'));
    }

    /**
     * Display the specified Event.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $this->authorize('is-editor');

        $event = $this->eventRepository->findWithoutFail($id);

        if (empty($event)) {
            Flash::error('Event not found');

            return redirect(route('events.index'));
        }

        return view('events.show')->with('event', $event);
    }

    /**
     * Show the form for editing the specified Event.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $this->authorize('is-editor');

        $event = $this->eventRepository->findWithoutFail($id);

        if (empty($event)) {
            Flash::error('Event not found');

            return redirect(route('events.index'));
        }


        return view('events.edit')->with('event', $event);
    }

    /**
     * Update the specified Event in storage.
     *
     * @param  int              $id
     * @param UpdateEventRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateEventRequest $request)
    {
        $this->authorize('is-editor');

        $event = $this->eventRepository->findWithoutFail($id);

        if (empty($event)) {
            Flash::error('Event not found');

            return redirect(route('events.index'));
        }

        $inputs = $request->all();
        if (isset($request->image)){
            $inputs['image'] = time() . rand(1000, 9999) . $request->file('image')->getClientOriginalName();
            $path = public_path() . '/uploads/events/image/';
            $request->image->move($path, $inputs['image']);
            Image::make($path . $inputs['image'])
                ->resize('500', '300')
                ->save(public_path() . '/uploads/events/image/thumbnail/' . $inputs['image']);
            $inputs['image_thumbnail'] = $inputs['image'];
        }

        $event = $this->eventRepository->update($inputs, $id);

        Flash::success('Event updated successfully.');

        return redirect(route('events.index'));
    }

    /**
     * Remove the specified Event from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $this->authorize('is-editor');

        $event = $this->eventRepository->findWithoutFail($id);

        if (empty($event)) {
            Flash::error('Event not found');

            return redirect(route('events.index'));
        }

        $this->eventRepository->delete($id);

        Flash::success('Event deleted successfully.');

        return redirect(route('events.index'));
    }
}
