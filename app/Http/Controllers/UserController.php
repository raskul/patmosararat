<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Laracasts\Flash\Flash;

class UserController extends Controller
{
    public function index()//ok
    {
        $this->authorize('is-admin');
        $users = User::orderByDesc('created_at')->paginate(10);

        return view('users.index', compact('users'));
    }

    public function create()//ok
    {
        $this->authorize('is-admin');

        return view('users.create');
    }

    public function store(Request $request)
    {
        $this->authorize('is-admin');

        $inputs = $request->all();

        if (isset($request->image)){
            $inputs['image'] = time() . rand(1000, 9999) . $request->file('image')->getClientOriginalName();
            $path = public_path() . '/uploads/users/image/';
            $request->image->move($path, $inputs['image']);
            Image::make($path . $inputs['image'])
                ->resize('500', '350')
                ->save(public_path() . '/uploads/users/image/thumbnail/' . $inputs['image']);
        }

        User::create($inputs);

        Flash::success('User saved successfully.');

        return redirect(route('users.index'));
    }

    public function show($id)//doesn't work
    {
        $this->authorize('is-admin');

        $sliderText = $this->sliderTextRepository->findWithoutFail($id);

        if (empty($sliderText)) {
            Flash::error('User Text not found');

            return redirect(route('sliderTexts.index'));
        }

        return view('slider_texts.show')->with('sliderText', $sliderText);
    }

    public function edit(User $user)//ok
    {
        $this->authorize('is-admin');

        if (empty($user)) {
            Flash::error('User not found');

            return redirect(route('users.index'));
        }

        return view('users.edit', compact('user'));
    }

    public function update(User $user, Request $request)
    {
        $this->authorize('is-admin');

        if (empty($user)) {
            Flash::error('User not found');

            return redirect(route('users.index'));
        }

        $inputs = $request->all();

        if (isset($request->image)){
            $inputs['image'] = time() . rand(1000, 9999) . $request->file('image')->getClientOriginalName();
            $path = public_path() . '/uploads/users/image/';
            $request->image->move($path, $inputs['image']);
            Image::make($path . $inputs['image'])
                ->resize('500', '350')
                ->save(public_path() . '/uploads/users/image/thumbnail/' . $inputs['image']);
        }

        $user->update($inputs);

        Flash::success('User updated successfully.');

        return redirect(route('users.index'));
    }

    public function destroy(User $user)//ok
    {
        $this->authorize('is-admin');

        if (empty($user)) {
            Flash::error('User not found');

            return redirect(route('users.index'));
        }

        $user->delete();

        Flash::success('User deleted successfully.');

        return redirect(route('users.index'));
    }
}
