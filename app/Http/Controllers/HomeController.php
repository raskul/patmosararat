<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $request_count = User::where('role_id', 2)->count();

        return view('home', compact('request_count'));
    }

    public function volunteerRequest()
    {
        $users = User::where('role_id', 2)->paginate(30);

        return view('users.index', compact('users'));
    }
}
