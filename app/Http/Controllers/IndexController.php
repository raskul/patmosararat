<?php

namespace App\Http\Controllers;

use App\Models\AboutUs;
use App\Models\Activity;
use App\Models\Contact;
use App\Models\Donate;
use App\Models\Event;
use App\Models\PhotoGallery;
use App\Models\Service;
use App\Models\SliderText;
use App\Models\Volunteer;
use App\User;
use Cookie;
use Illuminate\Http\Request;
use Laracasts\Flash\Flash;

class IndexController extends Controller
{
    public function lang_en()
    {
        Cookie::queue(Cookie::forget('lang'));

        return redirect()->back();
    }

    public function lang_am()
    {
        Cookie::queue(Cookie::forever('lang', 'am'));

        return redirect()->back();
    }

    public function index()
    {
        if (Cookie::get('lang') == 'am') {
            Cookie::queue(Cookie::forever('lang', 'am'));
            $lang = 'am';
        } else {
            Cookie::queue(Cookie::forget('lang'));
            $lang = false;
        }

        $slider_texts = SliderText::get();
        $about_us = AboutUs::get();
        $services = Service::take(8)->get();
        $activites = Activity::get();
        $events = Event::orderByDesc('created_at')->take(8)->get();

        return view('index.index', compact('lang', 'slider_texts', 'about_us', 'services', 'activites', 'events'));
    }

    public function about_us()
    {
        if (Cookie::get('lang') == 'am') {
            Cookie::queue(Cookie::forever('lang', 'am'));
            $lang = 'am';
        } else {
            Cookie::queue(Cookie::forget('lang'));
            $lang = false;
        }

        $slider_texts = SliderText::get();
        $about_us = AboutUs::get();

        return view('index.aboutus', compact('lang', 'slider_texts', 'about_us'));
    }

    public function contact()
    {
        if (Cookie::get('lang') == 'am') {
            Cookie::queue(Cookie::forever('lang', 'am'));
            $lang = 'am';
        } else {
            Cookie::queue(Cookie::forget('lang'));
            $lang = false;
        }

        $slider_texts = SliderText::get();
        $contact = Contact::get();


        return view('index.contact', compact('lang', 'slider_texts', 'contact'));
    }

    public function donate()
    {
        if (Cookie::get('lang') == 'am') {
            Cookie::queue(Cookie::forever('lang', 'am'));
            $lang = 'am';
        } else {
            Cookie::queue(Cookie::forget('lang'));
            $lang = false;
        }

        $slider_texts = SliderText::get();
        $donate = Donate::get();

        return view('index.donate', compact('lang', 'slider_texts', 'donate'));
    }

    public function staff()
    {
        if (Cookie::get('lang') == 'am') {
            Cookie::queue(Cookie::forever('lang', 'am'));
            $lang = 'am';
        } else {
            Cookie::queue(Cookie::forget('lang'));
            $lang = false;
        }

        $slider_texts = SliderText::get();

        return view('index.staff', compact('lang', 'slider_texts'));
    }

    public function activities()
    {
        if (Cookie::get('lang') == 'am') {
            Cookie::queue(Cookie::forever('lang', 'am'));
            $lang = 'am';
        } else {
            Cookie::queue(Cookie::forget('lang'));
            $lang = false;
        }

        $slider_texts = SliderText::get();
        $activites = Activity::get();

        return view('index.activities', compact('lang', 'slider_texts', 'activites'));
    }

    public function volunteer()
    {
        if (Cookie::get('lang') == 'am') {
            Cookie::queue(Cookie::forever('lang', 'am'));
            $lang = 'am';
        } else {
            Cookie::queue(Cookie::forget('lang'));
            $lang = false;
        }

        $slider_texts = SliderText::get();
        $volunteer = Volunteer::first();

        return view('index.volunteer', compact('lang', 'slider_texts', 'volunteer'));
    }

    public function volunteerApply(Request $request)
    {
        if (Cookie::get('lang') == 'am') {
            Cookie::queue(Cookie::forever('lang', 'am'));
            $lang = 'am';
        } else {
            Cookie::queue(Cookie::forget('lang'));
            $lang = false;
        }

        $slider_texts = SliderText::get();
        $activites = Activity::get();

        if ($request->isMethod('GET')) {

            return view('index.volunteerApply', compact('lang', 'slider_texts', 'activites'));
        } else {

            $inputs['name'] = $request->firstName;
            $inputs['first_name'] = $request->email;
            $inputs['last_name'] = $request->lastName;
            $inputs['birthday'] = $request->year . '-' . $request->month . '-' . $request->day;
            $inputs['email'] = time() . '_' . $request->email;
            $inputs['country'] = $request->country;
            $inputs['address'] = $request->address;
            $inputs['mobile'] = $request->mobile;
            $inputs['start_activity'] = $request->startDate;
            $inputs['end_activity'] = $request->endDate;
            $inputs['gender'] = $request->gender;

            if (isset($request->cv)) {
                $this->validate($request, [
                    'cv' => 'mimes:jpg,jpeg,zip,pdf|max:8500',
                ]);
                $fullName = 'uploads/cvs/' . time() . $request->file('cv')->getClientOriginalName();
                $request->file('cv')->move('uploads/cvs', $fullName);
                $inputs['upload'] = $fullName;
            }

            $inputs['role_id'] = 2;
            $inputs['password'] = bcrypt('volunteer');

            User::create($inputs);

            Flash::success('your request sent. we will call you. thank you.');

            return view('index.message', compact('lang', 'slider_texts', 'activites'));
        }
    }

    public function photoGallery(Event $event)
    {
        if (Cookie::get('lang') == 'am') {
            Cookie::queue(Cookie::forever('lang', 'am'));
            $lang = 'am';
        } else {
            Cookie::queue(Cookie::forget('lang'));
            $lang = false;
        }

        $slider_texts = SliderText::get();
        if ( ! (isset($event->id)) ) {
            $events = Event::orderByDesc('created_at')->paginate(16);
            return view('index.photo_gallery_second', compact('lang', 'slider_texts', 'events'));
        } else {
            $photos = PhotoGallery::where('event_id', $event->id)->paginate(16);
        }

        return view('index.photo_gallery', compact('lang', 'slider_texts', 'photos'));
    }
}
