<?php

namespace App\Http\Controllers;

use App\DataTables\SliderTextDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateSliderTextRequest;
use App\Http\Requests\UpdateSliderTextRequest;
use App\Repositories\SliderTextRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class SliderTextController extends AppBaseController
{
    /** @var  SliderTextRepository */
    private $sliderTextRepository;

    public function __construct(SliderTextRepository $sliderTextRepo)
    {
        $this->sliderTextRepository = $sliderTextRepo;
    }

    /**
     * Display a listing of the SliderText.
     *
     * @param SliderTextDataTable $sliderTextDataTable
     * @return Response
     */
    public function index(SliderTextDataTable $sliderTextDataTable)
    {
        $this->authorize('is-admin');

        return $sliderTextDataTable->render('slider_texts.index');
    }

    /**
     * Show the form for creating a new SliderText.
     *
     * @return Response
     */
    public function create()
    {
        $this->authorize('is-admin');

        return view('slider_texts.create');
    }

    /**
     * Store a newly created SliderText in storage.
     *
     * @param CreateSliderTextRequest $request
     *
     * @return Response
     */
    public function store(CreateSliderTextRequest $request)
    {
        $this->authorize('is-admin');

        $input = $request->all();

        $sliderText = $this->sliderTextRepository->create($input);

        Flash::success('Slider Text saved successfully.');

        return redirect(route('sliderTexts.index'));
    }

    /**
     * Display the specified SliderText.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $this->authorize('is-admin');

        $sliderText = $this->sliderTextRepository->findWithoutFail($id);

        if (empty($sliderText)) {
            Flash::error('Slider Text not found');

            return redirect(route('sliderTexts.index'));
        }

        return view('slider_texts.show')->with('sliderText', $sliderText);
    }

    /**
     * Show the form for editing the specified SliderText.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $this->authorize('is-admin');

        $sliderText = $this->sliderTextRepository->findWithoutFail($id);

        if (empty($sliderText)) {
            Flash::error('Slider Text not found');

            return redirect(route('sliderTexts.index'));
        }

        return view('slider_texts.edit')->with('sliderText', $sliderText);
    }

    /**
     * Update the specified SliderText in storage.
     *
     * @param  int              $id
     * @param UpdateSliderTextRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSliderTextRequest $request)
    {
        $this->authorize('is-admin');

        $sliderText = $this->sliderTextRepository->findWithoutFail($id);

        if (empty($sliderText)) {
            Flash::error('Slider Text not found');

            return redirect(route('sliderTexts.index'));
        }

        $sliderText = $this->sliderTextRepository->update($request->all(), $id);

        Flash::success('Slider Text updated successfully.');

        return redirect(route('sliderTexts.index'));
    }

    /**
     * Remove the specified SliderText from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $this->authorize('is-admin');

        $sliderText = $this->sliderTextRepository->findWithoutFail($id);

        if (empty($sliderText)) {
            Flash::error('Slider Text not found');

            return redirect(route('sliderTexts.index'));
        }

        $this->sliderTextRepository->delete($id);

        Flash::success('Slider Text deleted successfully.');

        return redirect(route('sliderTexts.index'));
    }
}
