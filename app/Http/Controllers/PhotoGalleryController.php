<?php

namespace App\Http\Controllers;

use App\DataTables\PhotoGalleryDataTable;
use App\Http\Requests;
use App\Http\Requests\CreatePhotoGalleryRequest;
use App\Http\Requests\UpdatePhotoGalleryRequest;
use App\Models\Event;
use App\Repositories\PhotoGalleryRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Intervention\Image\Facades\Image;
use Response;

class PhotoGalleryController extends AppBaseController
{
    /** @var  PhotoGalleryRepository */
    private $photoGalleryRepository;

    public function __construct(PhotoGalleryRepository $photoGalleryRepo)
    {
        $this->photoGalleryRepository = $photoGalleryRepo;
    }

    /**
     * Display a listing of the PhotoGallery.
     *
     * @param PhotoGalleryDataTable $photoGalleryDataTable
     * @return Response
     */
    public function index(PhotoGalleryDataTable $photoGalleryDataTable)
    {
        $this->authorize('is-editor');

        return $photoGalleryDataTable->render('photo_galleries.index');
    }

    /**
     * Show the form for creating a new PhotoGallery.
     *
     * @return Response
     */
    public function create()
    {
        $this->authorize('is-editor');

        $events = Event::orderByDesc('created_at')->get();

        return view('photo_galleries.create', compact('events'));
    }

    /**
     * Store a newly created PhotoGallery in storage.
     *
     * @param CreatePhotoGalleryRequest $request
     *
     * @return Response
     */
    public function store(CreatePhotoGalleryRequest $request)
    {
        $this->authorize('is-editor');

        $inputs = $request->all();

        $inputs['image'] = time() . rand(1000, 9999) . $request->file('image')->getClientOriginalName();
        $path = public_path() . '/uploads/events/image/';
        $request->image->move($path, $inputs['image']);
        Image::make($path . $inputs['image'])
            ->resize('500', '300')
            ->save(public_path() . '/uploads/events/image/thumbnail/' . $inputs['image']);
        $inputs['image_thumbnail'] = $inputs['image'];
        $inputs['type'] = 1 ;
        $inputs['who_create'] = auth()->user()->id ;

        $photoGallery = $this->photoGalleryRepository->create($inputs);

        Flash::success('Photo Gallery saved successfully.');

        return redirect(route('photoGalleries.index'));
    }

    /**
     * Display the specified PhotoGallery.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $this->authorize('is-editor');

        $photoGallery = $this->photoGalleryRepository->findWithoutFail($id);

        if (empty($photoGallery)) {
            Flash::error('Photo Gallery not found');

            return redirect(route('photoGalleries.index'));
        }

        return view('photo_galleries.show')->with('photoGallery', $photoGallery);
    }

    /**
     * Show the form for editing the specified PhotoGallery.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $this->authorize('is-editor');

        $events = Event::orderByDesc('created_at')->get();

        $photoGallery = $this->photoGalleryRepository->findWithoutFail($id);

        if (empty($photoGallery)) {
            Flash::error('Photo Gallery not found');

            return redirect(route('photoGalleries.index'));
        }

        return view('photo_galleries.edit', compact('photoGallery', 'events'));
    }

    /**
     * Update the specified PhotoGallery in storage.
     *
     * @param  int              $id
     * @param UpdatePhotoGalleryRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePhotoGalleryRequest $request)
    {
        $this->authorize('is-editor');

        $photoGallery = $this->photoGalleryRepository->findWithoutFail($id);

        if (empty($photoGallery)) {
            Flash::error('Photo Gallery not found');

            return redirect(route('photoGalleries.index'));
        }

        $inputs = $request->all();
        if (isset($request->image)){
            $inputs['image'] = time() . rand(1000, 9999) . $request->file('image')->getClientOriginalName();
            $path = public_path() . '/uploads/events/image/';
            $request->image->move($path, $inputs['image']);
            Image::make($path . $inputs['image'])
                ->resize('500', '300')
                ->save(public_path() . '/uploads/events/image/thumbnail/' . $inputs['image']);
            $inputs['image_thumbnail'] = $inputs['image'];
        }
        $inputs['type'] = 1 ;
        $inputs['who_create'] = auth()->user()->id ;

        $photoGallery = $this->photoGalleryRepository->update($inputs, $id);

        Flash::success('Photo Gallery updated successfully.');

        return redirect(route('photoGalleries.index'));
    }

    /**
     * Remove the specified PhotoGallery from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $this->authorize('is-editor');

        $photoGallery = $this->photoGalleryRepository->findWithoutFail($id);

        if (empty($photoGallery)) {
            Flash::error('Photo Gallery not found');

            return redirect(route('photoGalleries.index'));
        }

        $this->photoGalleryRepository->delete($id);

        Flash::success('Photo Gallery deleted successfully.');

        return redirect(route('photoGalleries.index'));
    }
}
