<?php

namespace App\Http\Middleware;

use Closure;
use Cookie;
use PharIo\Manifest\Url;

class Lang
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

//        if (!$request->secure()) {
////            dd($request->getRequestUri());
////            return redirect()->secure($request->getRequestUri());
//            URL::forceSchema('https');
//        }

        return $next($request);

    }
}
