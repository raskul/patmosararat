<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class AboutUs
 * @package App\Models
 * @version July 1, 2018, 2:36 pm UTC
 *
 * @property string text
 * @property string text_am
 */
class AboutUs extends Model
{

    public $table = 'aboutuses';
    


    public $fillable = [
        'text',
        'text_am'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'text' => 'string',
        'text_am' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
