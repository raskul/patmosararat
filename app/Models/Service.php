<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Service
 * @package App\Models
 * @version July 5, 2018, 10:07 am UTC
 *
 * @property string big_text
 * @property string small_text
 * @property string big_text_am
 * @property string small_text_am
 */
class Service extends Model
{

    public $table = 'services';
    


    public $fillable = [
        'big_text',
        'small_text',
        'big_text_am',
        'small_text_am'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'big_text' => 'string',
        'small_text' => 'string',
        'big_text_am' => 'string',
        'small_text_am' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
