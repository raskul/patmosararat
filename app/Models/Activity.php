<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Activity
 * @package App\Models
 * @version July 5, 2018, 12:07 pm UTC
 *
 * @property string left_text
 * @property string right_text
 * @property string left_text_am
 * @property string right_text_am
 */
class Activity extends Model
{

    public $table = 'activities';
    


    public $fillable = [
        'left_text',
        'right_text',
        'left_text_am',
        'right_text_am'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'left_text' => 'string',
        'right_text' => 'string',
        'left_text_am' => 'string',
        'right_text_am' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
