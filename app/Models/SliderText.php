<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class SliderText
 * @package App\Models
 * @version June 30, 2018, 8:23 pm UTC
 *
 * @property string big_text
 * @property string small_text
 * @property string big_text_am
 * @property string small_text_am
 */
class SliderText extends Model
{
//    use SoftDeletes;

    public $table = 'slider_texts';
    

//    protected $dates = ['deleted_at'];


    public $fillable = [
        'big_text',
        'small_text',
        'big_text_am',
        'small_text_am'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'big_text' => 'string',
        'small_text' => 'string',
        'big_text_am' => 'string',
        'small_text_am' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
