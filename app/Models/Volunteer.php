<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Volunteer
 * @package App\Models
 * @version July 21, 2018, 10:17 pm UTC
 *
 * @property string text
 * @property string text_am
 */
class Volunteer extends Model
{

    public $table = 'volunteers';
    


    public $fillable = [
        'text',
        'text_am'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'text' => 'string',
        'text_am' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
