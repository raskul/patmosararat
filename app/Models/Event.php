<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Event
 * @package App\Models
 * @version July 5, 2018, 5:48 pm UTC
 *
 * @property string name
 * @property string image
 * @property string image_thumbnail
 * @property string time
 */
class Event extends Model
{

    public $table = 'events';
    


    public $fillable = [
        'name',
        'image',
        'image_thumbnail',
        'time'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'image' => 'string',
        'image_thumbnail' => 'string',
        'time' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
