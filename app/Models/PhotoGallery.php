<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class PhotoGallery
 * @package App\Models
 * @version July 10, 2018, 4:11 pm UTC
 *
 * @property string event_id
 * @property string image
 * @property string image_thumbnail
 * @property string who_create
 * @property string description
 * @property string type
 */
class PhotoGallery extends Model
{

    public $table = 'photo_galleries';
    


    public $fillable = [
        'event_id',
        'image',
        'image_thumbnail',
        'who_create',
        'description',
        'type'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'event_id' => 'string',
        'image' => 'string',
        'image_thumbnail' => 'string',
        'who_create' => 'string',
        'description' => 'string',
        'type' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
