<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Donate
 * @package App\Models
 * @version July 9, 2018, 6:31 am UTC
 *
 * @property string text
 * @property string text_am
 */
class Donate extends Model
{

    public $table = 'donates';
    


    public $fillable = [
        'text',
        'text_am'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'text' => 'string',
        'text_am' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
