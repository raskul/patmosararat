<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Contact
 * @package App\Models
 * @version July 4, 2018, 11:55 am UTC
 *
 * @property string text
 * @property string text_am
 */
class Contact extends Model
{

    public $table = 'contacts';
    


    public $fillable = [
        'text',
        'text_am'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'text' => 'string',
        'text_am' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
