<?php

namespace App\Repositories;

use App\Models\Donate;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class DonateRepository
 * @package App\Repositories
 * @version July 9, 2018, 6:31 am UTC
 *
 * @method Donate findWithoutFail($id, $columns = ['*'])
 * @method Donate find($id, $columns = ['*'])
 * @method Donate first($columns = ['*'])
*/
class DonateRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'text',
        'text_am'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Donate::class;
    }
}
