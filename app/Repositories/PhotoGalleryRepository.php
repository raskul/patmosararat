<?php

namespace App\Repositories;

use App\Models\PhotoGallery;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class PhotoGalleryRepository
 * @package App\Repositories
 * @version July 10, 2018, 4:11 pm UTC
 *
 * @method PhotoGallery findWithoutFail($id, $columns = ['*'])
 * @method PhotoGallery find($id, $columns = ['*'])
 * @method PhotoGallery first($columns = ['*'])
*/
class PhotoGalleryRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'event_id',
        'image',
        'image_thumbnail',
        'who_create',
        'description',
        'type'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PhotoGallery::class;
    }
}
