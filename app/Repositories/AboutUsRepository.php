<?php

namespace App\Repositories;

use App\Models\AboutUs;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class AboutUsRepository
 * @package App\Repositories
 * @version July 1, 2018, 2:36 pm UTC
 *
 * @method AboutUs findWithoutFail($id, $columns = ['*'])
 * @method AboutUs find($id, $columns = ['*'])
 * @method AboutUs first($columns = ['*'])
*/
class AboutUsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'text',
        'text_am'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return AboutUs::class;
    }
}
