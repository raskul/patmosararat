<?php

namespace App\Repositories;

use App\Models\SliderText;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class SliderTextRepository
 * @package App\Repositories
 * @version June 30, 2018, 8:23 pm UTC
 *
 * @method SliderText findWithoutFail($id, $columns = ['*'])
 * @method SliderText find($id, $columns = ['*'])
 * @method SliderText first($columns = ['*'])
*/
class SliderTextRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'big_text',
        'small_text',
        'big_text_am',
        'small_text_am'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return SliderText::class;
    }
}
