<?php

namespace App\Repositories;

use App\Models\Volunteer;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class VolunteerRepository
 * @package App\Repositories
 * @version July 21, 2018, 10:17 pm UTC
 *
 * @method Volunteer findWithoutFail($id, $columns = ['*'])
 * @method Volunteer find($id, $columns = ['*'])
 * @method Volunteer first($columns = ['*'])
*/
class VolunteerRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'text',
        'text_am'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Volunteer::class;
    }
}
