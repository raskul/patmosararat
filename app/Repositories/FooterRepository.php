<?php

namespace App\Repositories;

use App\Models\Footer;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class FooterRepository
 * @package App\Repositories
 * @version July 21, 2018, 10:17 pm UTC
 *
 * @method Footer findWithoutFail($id, $columns = ['*'])
 * @method Footer find($id, $columns = ['*'])
 * @method Footer first($columns = ['*'])
*/
class FooterRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'text',
        'text_am'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Footer::class;
    }
}
